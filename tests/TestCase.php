<?php

/** @noinspection PhpUnhandledExceptionInspection */

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * @internal
 * @coversNothing
 */
class TestCase extends KernelTestCase
{
    /**
     * Loads data provider from a yaml file
     * File should be:
     *   - in a directory, inside the providers directory, with the same name as the class in which the test is situated
     *   - yaml with the same name as the test method.
     *
     * @throws \Exception
     */
    public function yamlFile(string $testMethodName): array
    {
        $calledClass = static::class;
        /** @noinspection PhpUndefinedClassInspection */
        $object = new \ReflectionClass(new $calledClass());

        $className = explode('\\', $calledClass);
        $className = $className[\count($className) - 1];

        $yamlFilePath = sprintf('/providers/%s/%s.yaml', $className, $testMethodName);
        $dir = \dirname($object->getFileName()).$yamlFilePath;

        return Yaml::parse(file_get_contents($dir));
    }

    /**
     * @param mixed $object
     *
     * @return mixed
     */
    protected function callRestrictedMethod($object, string $methodName, array $params = [])
    {
        $method = $this->getProtectedMethod($object, $methodName);

        return $method->invokeArgs(\is_object($object) ? $object : null, $params);
    }

    /**
     * @param string $object
     * @noinspection PhpDocMissingThrowsInspection
     */
    protected function getProtectedMethod($object, string $methodName): ?\ReflectionMethod
    {
        $method = null;

        /** @noinspection PhpUndefinedClassInspection */
        $reflector = new \ReflectionClass($object);

        $method = $reflector->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * In case of a private property, the depth parameter should be used. For example, a call with depth = 1 will be
     * able to get a private property from the 1st parent.
     *
     * @param mixed $object
     *
     * @return mixed
     */
    protected function getRestrictedPropertyValue($object, string $propertyName, int $depth = 0)
    {
        return $this->getInheritedProtectedProperty($object, $propertyName, $depth)->getValue($object);
    }

    /**
     * In case of a private property, the depth parameter should be used. For example, a call with depth = 1 will be
     * able to set a private property from the 1st parent.
     *
     * @param mixed $object
     * @param mixed $value
     */
    protected function setRestrictedPropertyValue($object, string $propertyName, $value, int $depth = 0): void
    {
        $this->getInheritedProtectedProperty($object, $propertyName, $depth)->setValue($object, $value);
    }

    /**
     * @param object $object
     * @noinspection PhpDocMissingThrowsInspection
     */
    protected function getInheritedProtectedProperty($object, string $propertyName, int $depth): \ReflectionProperty
    {
        $property = null;

        /** @noinspection PhpUndefinedClassInspection */
        $reflector = new \ReflectionClass($object);
        for ($i = 0; $i < $depth; ++$i) {
            $reflector = $reflector->getParentClass();
        }

        $property = $reflector->getProperty($propertyName);
        $property->setAccessible(true);

        return $property;
    }
}
