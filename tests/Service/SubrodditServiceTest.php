<?php

namespace App\Tests\Service;

use App\Entity\Subroddit;
use App\Exception\InvalidEntityException;
use App\Repository\SubrodditRepository;
use App\Service\SubrodditService;
use App\Tests\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Service\AbstractService
 * @covers \App\Service\SubrodditService
 */
class SubrodditServiceTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $validatorMock = \Mockery::mock(ValidatorInterface::class);

        $service = new SubrodditService($objectManagerMock, $subrodditRepositoryMock, $validatorMock);

        $result = $this->getRestrictedPropertyValue($service, 'entityManager');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'subrodditRepository');
        $this->assertSame($subrodditRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'validator');
        $this->assertSame($validatorMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function createSuccessfully(): void
    {
        $data = ['name' => 'Romania', 'shortDesc' => 'Many ROdditors'];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnFalse();
        $objectManagerMock->shouldReceive('persist')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $service = new SubrodditService($objectManagerMock, $subrodditRepositoryMock, $validatorMock);

        $result = $service->create($data);
        $this->assertInstanceOf(Subroddit::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame($data['name'], $result->getName());
        $this->assertSame($data['shortDesc'], $result->getShortDesc());
        $this->assertSame('', $result->getLogo());
        $this->assertCount(0, $result->getModerators()->getValues());
        $this->assertCount(0, $result->getSubscriptions()->getValues());
        $this->assertCount(0, $result->getTopics()->getValues());
    }

    /**
     * @test
     */
    public function createWithInvalidEntityException(): void
    {
        $data = ['name' => 'Romania', 'shortDesc' => 'Many ROdditors'];

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);

        $service = new SubrodditService($objectManagerMock, $subrodditRepositoryMock, $validatorMock);

        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->create($data);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function updateShortDescSuccessfully(): void
    {
        $data = ['id' => 547, 'shortDesc' => 'Many ROdditors'];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnTrue();
        $objectManagerMock->shouldReceive('flush')->once();

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $service = new SubrodditService($objectManagerMock, $subrodditRepositoryMock, $validatorMock);

        $result = $service->update($data);
        $this->assertInstanceOf(Subroddit::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame('', $result->getName());
        $this->assertSame($data['shortDesc'], $result->getShortDesc());
        $this->assertSame('', $result->getLogo());
        $this->assertCount(0, $result->getModerators()->getValues());
        $this->assertCount(0, $result->getSubscriptions()->getValues());
        $this->assertCount(0, $result->getTopics()->getValues());
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function updateLogoSuccessfully(): void
    {
        $data = ['id' => 547, 'logo' => 'romania.png'];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnTrue();
        $objectManagerMock->shouldReceive('flush')->once();

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $service = new SubrodditService($objectManagerMock, $subrodditRepositoryMock, $validatorMock);

        $result = $service->update($data);
        $this->assertInstanceOf(Subroddit::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame('', $result->getName());
        $this->assertSame('', $result->getShortDesc());
        $this->assertSame($data['logo'], $result->getLogo());
        $this->assertCount(0, $result->getModerators()->getValues());
        $this->assertCount(0, $result->getSubscriptions()->getValues());
        $this->assertCount(0, $result->getTopics()->getValues());
    }

    /**
     * @test
     */
    public function updateWithInvalidEntityException(): void
    {
        $data = ['id' => 315];

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $service = new SubrodditService($objectManagerMock, $subrodditRepositoryMock, $validatorMock);

        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->update($data);
    }
}
