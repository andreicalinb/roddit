<?php

namespace App\Tests\Service;

use App\Entity\Subroddit;
use App\Entity\Topic;
use App\Entity\User;
use App\Exception\InvalidEntityException;
use App\Repository\SubrodditRepository;
use App\Repository\UserRepository;
use App\Service\TopicService;
use App\Tests\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Service\AbstractService
 * @covers \App\Service\TopicService
 */
class TopicServiceTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $validatorMock = \Mockery::mock(ValidatorInterface::class);

        $service = new TopicService($objectManagerMock, $subrodditRepositoryMock, $userRepositoryMock, $validatorMock);

        $result = $this->getRestrictedPropertyValue($service, 'entityManager');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'subrodditRepository');
        $this->assertSame($subrodditRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'userRepository');
        $this->assertSame($userRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'validator');
        $this->assertSame($validatorMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function createSuccessfully(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321, 'title' => 'The Best', 'description' => 'Not needed'];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnFalse();
        $objectManagerMock->shouldReceive('persist')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new TopicService($objectManagerMock, $subrodditRepositoryMock, $userRepositoryMock, $validatorMock);

        $result = $service->create($data);
        $this->assertInstanceOf(Topic::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame($data['title'], $result->getTitle());
        $this->assertSame($data['description'], $result->getDescription());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getCreated());
        $this->assertSame($user, $result->getOp());
        $this->assertSame($subroddit, $result->getSubroddit());
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function createWithInvalidEntityException(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321, 'title' => 'The Best', 'description' => 'Not needed'];

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new TopicService($objectManagerMock, $subrodditRepositoryMock, $userRepositoryMock, $validatorMock);

        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->create($data);
    }
}
