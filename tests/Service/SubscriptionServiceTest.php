<?php

namespace App\Tests\Service;

use App\Entity\Subroddit;
use App\Entity\Subscription;
use App\Entity\User;
use App\Exception\InvalidEntityException;
use App\Repository\SubrodditRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\UserRepository;
use App\Service\SubscriptionService;
use App\Tests\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Service\AbstractService
 * @covers \App\Service\SubscriptionService
 */
class SubscriptionServiceTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $validatorMock = \Mockery::mock(ValidatorInterface::class);

        $service = new SubscriptionService(
            $objectManagerMock,
            $subscriptionRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $result = $this->getRestrictedPropertyValue($service, 'entityManager');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'subscriptionRepository');
        $this->assertSame($subscriptionRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'subrodditRepository');
        $this->assertSame($subrodditRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'userRepository');
        $this->assertSame($userRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'validator');
        $this->assertSame($validatorMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function subscribeSuccessfully(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnFalse();
        $objectManagerMock->shouldReceive('persist')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new SubscriptionService(
            $objectManagerMock,
            $subscriptionRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $result = $service->subscribe($data);
        $this->assertInstanceOf(Subscription::class, $result);
        $this->assertSame($subroddit, $result->getSubroddit());
        $this->assertSame($user, $result->getUser());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getSince());
        $this->assertSame('', $result->getFlair());
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function subscribeWithInvalidEntityException(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321];

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);

        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new SubscriptionService(
            $objectManagerMock,
            $subscriptionRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->subscribe($data);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function unsubscribeSuccessfully(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321];

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('remove')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $subscription = new Subscription(new User(), new Subroddit());
        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);
        $subscriptionRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subscription);

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);

        $service = new SubscriptionService(
            $objectManagerMock,
            $subscriptionRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $service->unsubscribe($data);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function updateFlairSuccessfully(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321, 'flair' => 'Muhaha'];

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnTrue();
        $objectManagerMock->shouldReceive('flush')->once();

        $user = new User();
        $subroddit = new Subroddit();
        $subscription = new Subscription($user, $subroddit);
        $since = new \DateTime();
        $subscription->setSince($since);
        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);
        $subscriptionRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subscription);

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);

        $service = new SubscriptionService(
            $objectManagerMock,
            $subscriptionRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $result = $service->updateFlair($data);
        $this->assertInstanceOf(Subscription::class, $result);
        $this->assertSame($subroddit, $result->getSubroddit());
        $this->assertSame($user, $result->getUser());
        $this->assertSame($since, $result->getSince());
        $this->assertSame($data['flair'], $result->getFlair());
    }
}
