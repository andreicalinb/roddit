<?php

namespace App\Tests\Service;

use App\Entity\Moderator;
use App\Entity\Subroddit;
use App\Entity\User;
use App\Exception\InvalidEntityException;
use App\Repository\ModeratorRepository;
use App\Repository\SubrodditRepository;
use App\Repository\UserRepository;
use App\Service\ModeratorService;
use App\Tests\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Service\AbstractService
 * @covers \App\Service\ModeratorService
 */
class ModeratorServiceTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $moderatorRepositoryMock = \Mockery::mock(ModeratorRepository::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $validatorMock = \Mockery::mock(ValidatorInterface::class);

        $service = new ModeratorService(
            $objectManagerMock,
            $moderatorRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $result = $this->getRestrictedPropertyValue($service, 'entityManager');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'moderatorRepository');
        $this->assertSame($moderatorRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'subrodditRepository');
        $this->assertSame($subrodditRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'userRepository');
        $this->assertSame($userRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'validator');
        $this->assertSame($validatorMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function createSuccessfully(): void
    {
        $data = ['subrodditId' => 544, 'userId' => 455];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnFalse();
        $objectManagerMock->shouldReceive('persist')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $moderatorRepositoryMock = \Mockery::mock(ModeratorRepository::class);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new ModeratorService(
            $objectManagerMock,
            $moderatorRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $result = $service->register($data);
        $this->assertInstanceOf(Moderator::class, $result);
        $this->assertSame($subroddit, $result->getSubroddit());
        $this->assertSame($user, $result->getUser());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getSince());
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function createWithInvalidEntityException(): void
    {
        $data = ['subrodditId' => 544, 'userId' => 455];

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $moderatorRepositoryMock = \Mockery::mock(ModeratorRepository::class);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($subroddit);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new ModeratorService(
            $objectManagerMock,
            $moderatorRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );
        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->register($data);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function unsubscribeSuccessfully(): void
    {
        $data = ['subrodditId' => 123, 'userId' => 321];

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('remove')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);

        $moderator = new Moderator(new User(), new Subroddit());
        $moderatorRepositoryMock = \Mockery::mock(ModeratorRepository::class);
        $moderatorRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($moderator);

        $service = new ModeratorService(
            $objectManagerMock,
            $moderatorRepositoryMock,
            $subrodditRepositoryMock,
            $userRepositoryMock,
            $validatorMock
        );

        $service->unregister($data);
        $this->assertTrue(true);
    }
}
