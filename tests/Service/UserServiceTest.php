<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Exception\InvalidEntityException;
use App\Repository\UserRepository;
use App\Service\UserService;
use App\Tests\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Service\AbstractService
 * @covers \App\Service\UserService
 */
class UserServiceTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $passwordEncoderMock = \Mockery::mock(UserPasswordEncoderInterface::class);
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $validatorMock = \Mockery::mock(ValidatorInterface::class);

        $service = new UserService($objectManagerMock, $passwordEncoderMock, $userRepositoryMock, $validatorMock);

        $result = $this->getRestrictedPropertyValue($service, 'entityManager');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'encoder');
        $this->assertSame($passwordEncoderMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'userRepository');
        $this->assertSame($userRepositoryMock, $result);

        $result = $this->getRestrictedPropertyValue($service, 'validator');
        $this->assertSame($validatorMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function createSuccessfully(): void
    {
        $password = 'bla-bla-bla123';
        $data = ['email' => 'bla@bla.bl', 'username' => 'Blanao', 'password' => $password];

        $passwordEncoderMock = \Mockery::mock(UserPasswordEncoderInterface::class);
        $passwordEncoderMock->shouldReceive('encodePassword')->once()->andReturn($password);

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnFalse();
        $objectManagerMock->shouldReceive('persist')->once();
        $objectManagerMock->shouldReceive('flush')->once();

        $userRepositoryMock = \Mockery::mock(UserRepository::class);

        $service = new UserService($objectManagerMock, $passwordEncoderMock, $userRepositoryMock, $validatorMock);

        $result = $service->create($data);
        $this->assertInstanceOf(User::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame($data['email'], $result->getEmail());
        $this->assertSame($data['username'], $result->getUsername());
        $this->assertSame($password, $result->getPassword());
        $this->assertTrue($result->getIsActive());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getCreated());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getModified());
        $this->assertCount(0, $result->getModerators()->getValues());
        $this->assertCount(0, $result->getSubscriptions()->getValues());
        $this->assertCount(0, $result->getTopics()->getValues());
    }

    /**
     * @test
     */
    public function createWithInvalidEntityException(): void
    {
        $password = 'bla-bla-bla123';
        $data = ['email' => 'bla@bla.bl', 'username' => 'Blanao', 'password' => $password];

        $passwordEncoderMock = \Mockery::mock(UserPasswordEncoderInterface::class);
        $passwordEncoderMock->shouldReceive('encodePassword')->once()->andReturn($password);

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);

        $userRepositoryMock = \Mockery::mock(UserRepository::class);

        $service = new UserService($objectManagerMock, $passwordEncoderMock, $userRepositoryMock, $validatorMock);

        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->create($data);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function updatePasswordSuccessfully(): void
    {
        $password = 'bla-bla-bla123';
        $data = ['id' => 333, 'password' => $password];

        $passwordEncoderMock = \Mockery::mock(UserPasswordEncoderInterface::class);
        $passwordEncoderMock->shouldReceive('encodePassword')->once()->andReturn($password);

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnTrue();
        $objectManagerMock->shouldReceive('flush')->once();

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new UserService($objectManagerMock, $passwordEncoderMock, $userRepositoryMock, $validatorMock);

        $result = $service->update($data);
        $this->assertInstanceOf(User::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame('', $result->getEmail());
        $this->assertSame('', $result->getUsername());
        $this->assertSame($password, $result->getPassword());
        $this->assertTrue($result->getIsActive());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getCreated());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getModified());
        $this->assertCount(0, $result->getModerators()->getValues());
        $this->assertCount(0, $result->getSubscriptions()->getValues());
        $this->assertCount(0, $result->getTopics()->getValues());
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function updateEmailSuccessfully(): void
    {
        $email = 'ooo@ppp.qq';
        $data = ['id' => 333, 'email' => $email];

        $passwordEncoderMock = \Mockery::mock(UserPasswordEncoderInterface::class);

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn([]);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('contains')->once()->andReturnTrue();
        $objectManagerMock->shouldReceive('flush')->once();

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new UserService($objectManagerMock, $passwordEncoderMock, $userRepositoryMock, $validatorMock);

        $result = $service->update($data);
        $this->assertInstanceOf(User::class, $result);
        $this->assertSame(0, $result->getId());
        $this->assertSame($email, $result->getEmail());
        $this->assertSame('', $result->getUsername());
        $this->assertSame('', $result->getPassword());
        $this->assertTrue($result->getIsActive());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getCreated());
        $this->assertInstanceOf(\DateTimeInterface::class, $result->getModified());
        $this->assertCount(0, $result->getModerators()->getValues());
        $this->assertCount(0, $result->getSubscriptions()->getValues());
        $this->assertCount(0, $result->getTopics()->getValues());
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function updateWithInvalidEntityException(): void
    {
        $data = ['id' => 315];

        $passwordEncoderMock = \Mockery::mock(UserPasswordEncoderInterface::class);

        $message = 'bad';
        $constraintViolation = \Mockery::mock(ConstraintViolation::class);
        $constraintViolation->shouldReceive('getMessage')->once()->andReturn($message);

        $anotherMessage = 'ra';
        $anotherConstraintViolation = \Mockery::mock(ConstraintViolation::class);
        $anotherConstraintViolation->shouldReceive('getMessage')->once()->andReturn($anotherMessage);

        $errors = [$constraintViolation, $anotherConstraintViolation];

        $validatorMock = \Mockery::mock(ValidatorInterface::class);
        $validatorMock->shouldReceive('validate')->once()->andReturn($errors);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')->once()->andReturn($user);

        $service = new UserService($objectManagerMock, $passwordEncoderMock, $userRepositoryMock, $validatorMock);

        $this->expectException(InvalidEntityException::class);
        $this->expectExceptionMessage(sprintf('%s,%s', $message, $anotherMessage));
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);

        $service->update($data);
    }
}
