<?php

namespace App\Tests\Monolog;

use App\Monolog\Logger;
use App\Tests\TestCase;
use Monolog\Logger as MonologLogger;

/**
 * @internal
 * @covers \App\Monolog\Logger
 */
class LoggerTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $loggerMock = \Mockery::mock(MonologLogger::class);
        $logger = new Logger($loggerMock);

        $result = $this->getRestrictedPropertyValue($logger, 'logger');
        $this->assertSame($loggerMock, $result);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function log(string $method, string $level): void
    {
        $message = 'bla';
        $context = ['42' => 'answer'];
        $module = self::class;

        $expectedContext = $context + ['mypid' => getmypid(), 'session_id' => null, 'module' => $module];
        $loggerMock = \Mockery::mock(MonologLogger::class);
        $loggerMock->shouldReceive('addRecord')
            ->once()
            ->with(\constant($level), $message, $expectedContext)
            ->andReturnTrue();

        $logger = new Logger($loggerMock);

        $result = $logger->{$method}($message, $context, $module);
        $this->assertTrue($result);
    }
}
