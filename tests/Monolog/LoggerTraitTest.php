<?php

namespace App\Tests\Monolog;

use App\Monolog\Logger;
use App\Monolog\LoggerTrait;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Monolog\LoggerTrait
 */
class LoggerTraitTest extends TestCase
{
    /**
     * @test
     * @dataProvider yamlFile
     */
    public function logWithoutException(string $testedMethod, string $expectedMethod): void
    {
        $constant = 'Uaaaaaa';
        $context = ['101' => '010'];

        $loggerTraitMock = $this->getMockForTrait(LoggerTrait::class, [], '', true, true, true, ['getClassConstant']);
        $loggerTraitMock->expects($this->once())
            ->method('getClassConstant')
            ->willReturn($constant);

        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive($expectedMethod)
            ->once()
            ->with($constant, $context, $loggerTraitMock)
            ->andReturnTrue();

        $this->setRestrictedPropertyValue($loggerTraitMock, 'logger', $loggerMock);

        $this->callRestrictedMethod($loggerTraitMock, $testedMethod, [$context]);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function logWithException(string $testedMethod, string $expectedMethod): void
    {
        $constant = 'Uaaaaaa';
        $context = ['101' => '010'];
        $expectedContext = $context;

        $message = 'I always cry';
        $code = 400;
        $exception = new \Exception($message, $code);
        $context['exception'] = $exception;

        $loggerTraitMock = $this->getMockForTrait(LoggerTrait::class, [], '', true, true, true, ['getClassConstant']);
        $loggerTraitMock->expects($this->once())
            ->method('getClassConstant')
            ->willReturn($constant);

        $expectedContext['exception_message'] = $message;
        $expectedContext['exception_code'] = $code;
        $expectedContext['exception_type'] = \Exception::class;

        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive($expectedMethod)
            ->once()
            ->with($constant, $expectedContext, $loggerTraitMock)
            ->andReturnTrue();

        $this->setRestrictedPropertyValue($loggerTraitMock, 'logger', $loggerMock);

        $this->callRestrictedMethod($loggerTraitMock, $testedMethod, [$context]);
    }
}
