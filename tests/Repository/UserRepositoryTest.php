<?php

namespace App\Tests\Repository;

use App\Entity\User;
use App\Exception\NonExistentUserException;
use App\Repository\UserRepository;
use App\Tests\TestCase;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 * @covers \App\Repository\UserRepository
 */
class UserRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new UserRepository($managerRegistryMock);

        $result = $this->getRestrictedPropertyValue($repository, '_em');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($repository, '_class');
        $this->assertSame($classMetadataMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function findOneBySuccessfully(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $userMock = \Mockery::mock(User::class);

        $entityPersisterMock = \Mockery::mock(EntityPersister::class);
        $entityPersisterMock->shouldReceive('load')->once()->andReturn($userMock);

        $unitOfWorkMock = \Mockery::mock(UnitOfWork::class);
        $unitOfWorkMock->shouldReceive('getEntityPersister')->once()->andReturn($entityPersisterMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('getUnitOfWork')->once()->andReturn($unitOfWorkMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new UserRepository($managerRegistryMock);

        $result = $repository->findOneBy([]);
        $this->assertSame($userMock, $result);
    }

    /**
     * @test
     */
    public function findOneByWithNonExistentUserException(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $entityPersisterMock = \Mockery::mock(EntityPersister::class);
        $entityPersisterMock->shouldReceive('load')->once()->andReturnNull();

        $unitOfWorkMock = \Mockery::mock(UnitOfWork::class);
        $unitOfWorkMock->shouldReceive('getEntityPersister')->once()->andReturn($entityPersisterMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('getUnitOfWork')->once()->andReturn($unitOfWorkMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new UserRepository($managerRegistryMock);

        $this->expectException(NonExistentUserException::class);
        $this->expectExceptionMessage('User does not exist.');
        $this->expectExceptionCode(Response::HTTP_NOT_FOUND);

        $repository->findOneBy([]);
    }
}
