<?php

namespace App\Tests\Repository;

use App\Repository\TopicRepository;
use App\Tests\TestCase;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * @internal
 * @covers \App\Repository\TopicRepository
 */
class TopicRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new TopicRepository($managerRegistryMock);

        $result = $this->getRestrictedPropertyValue($repository, '_em');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($repository, '_class');
        $this->assertSame($classMetadataMock, $result);
    }
}
