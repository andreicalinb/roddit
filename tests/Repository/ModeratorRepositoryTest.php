<?php

namespace App\Tests\Repository;

use App\Entity\Moderator;
use App\Exception\NonExistentModeratorException;
use App\Repository\ModeratorRepository;
use App\Tests\TestCase;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 * @covers \App\Repository\ModeratorRepository
 */
class ModeratorRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new ModeratorRepository($managerRegistryMock);

        $result = $this->getRestrictedPropertyValue($repository, '_em');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($repository, '_class');
        $this->assertSame($classMetadataMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function findOneBySuccessfully(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $moderatorMock = \Mockery::mock(Moderator::class);

        $entityPersisterMock = \Mockery::mock(EntityPersister::class);
        $entityPersisterMock->shouldReceive('load')->once()->andReturn($moderatorMock);

        $unitOfWorkMock = \Mockery::mock(UnitOfWork::class);
        $unitOfWorkMock->shouldReceive('getEntityPersister')->once()->andReturn($entityPersisterMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('getUnitOfWork')->once()->andReturn($unitOfWorkMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new ModeratorRepository($managerRegistryMock);

        $result = $repository->findOneBy([]);
        $this->assertSame($moderatorMock, $result);
    }

    /**
     * @test
     */
    public function findOneByWithNonExistentModeratorException(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $entityPersisterMock = \Mockery::mock(EntityPersister::class);
        $entityPersisterMock->shouldReceive('load')->once()->andReturnNull();

        $unitOfWorkMock = \Mockery::mock(UnitOfWork::class);
        $unitOfWorkMock->shouldReceive('getEntityPersister')->once()->andReturn($entityPersisterMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('getUnitOfWork')->once()->andReturn($unitOfWorkMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new ModeratorRepository($managerRegistryMock);

        $this->expectException(NonExistentModeratorException::class);
        $this->expectExceptionMessage('Moderator does not exist.');
        $this->expectExceptionCode(Response::HTTP_NOT_FOUND);

        $repository->findOneBy([]);
    }
}
