<?php

namespace App\Tests\Repository;

use App\Entity\Subroddit;
use App\Exception\NonExistentSubrodditException;
use App\Repository\SubrodditRepository;
use App\Tests\TestCase;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 * @covers \App\Repository\SubrodditRepository
 */
class SubrodditRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new SubrodditRepository($managerRegistryMock);

        $result = $this->getRestrictedPropertyValue($repository, '_em');
        $this->assertSame($objectManagerMock, $result);

        $result = $this->getRestrictedPropertyValue($repository, '_class');
        $this->assertSame($classMetadataMock, $result);
    }

    /**
     * @test
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function findOneBySuccessfully(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $subrodditMock = \Mockery::mock(Subroddit::class);

        $entityPersisterMock = \Mockery::mock(EntityPersister::class);
        $entityPersisterMock->shouldReceive('load')->once()->andReturn($subrodditMock);

        $unitOfWorkMock = \Mockery::mock(UnitOfWork::class);
        $unitOfWorkMock->shouldReceive('getEntityPersister')->once()->andReturn($entityPersisterMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('getUnitOfWork')->once()->andReturn($unitOfWorkMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new SubrodditRepository($managerRegistryMock);

        $result = $repository->findOneBy([]);
        $this->assertSame($subrodditMock, $result);
    }

    /**
     * @test
     */
    public function findOneByWithNonExistentSubrodditException(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $entityPersisterMock = \Mockery::mock(EntityPersister::class);
        $entityPersisterMock->shouldReceive('load')->once()->andReturnNull();

        $unitOfWorkMock = \Mockery::mock(UnitOfWork::class);
        $unitOfWorkMock->shouldReceive('getEntityPersister')->once()->andReturn($entityPersisterMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('getUnitOfWork')->once()->andReturn($unitOfWorkMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new SubrodditRepository($managerRegistryMock);

        $this->expectException(NonExistentSubrodditException::class);
        $this->expectExceptionMessage('Subroddit does not exist.');
        $this->expectExceptionCode(Response::HTTP_NOT_FOUND);

        $repository->findOneBy([]);
    }

    /**
     * @test
     */
    public function findByModeratorIdSuccessfully(): void
    {
        $classMetadataMock = \Mockery::mock(ClassMetadata::class);

        $subrodditMock = \Mockery::mock(Subroddit::class);

        $queryMock = \Mockery::mock(AbstractQuery::class);
        $queryMock->shouldReceive('execute')->once()->andReturn([$subrodditMock]);

        $queryBuilderMock = \Mockery::mock(QueryBuilder::class);
        $queryBuilderMock->shouldReceive('select')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('from')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('innerJoin')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('where')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('setParameter')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('setFirstResult')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('setMaxResults')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('getQuery')->once()->andReturn($queryMock);

        $objectManagerMock = \Mockery::mock(EntityManagerInterface::class);
        $objectManagerMock->shouldReceive('getClassMetadata')->once()->andReturn($classMetadataMock);
        $objectManagerMock->shouldReceive('createQueryBuilder')->once()->andReturn($queryBuilderMock);

        $managerRegistryMock = \Mockery::mock(ManagerRegistry::class);
        $managerRegistryMock->shouldReceive('getManagerForClass')->once()->andReturn($objectManagerMock);

        $repository = new SubrodditRepository($managerRegistryMock);

        $result = $repository->findByModeratorId(123, 1, 8);
        $this->assertSame([$subrodditMock], $result);
    }
}
