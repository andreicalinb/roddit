<?php

namespace App\Tests\Exception;

use App\Exception\NonExistentUserException;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Exception\NonExistentUserException
 */
class NonExistentUserExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $message = 'I\'m hungry';
        $exception = new NonExistentUserException($message);

        $this->assertSame(sprintf('User does not exist. %s', $message), $exception->getMessage());
    }
}
