<?php

namespace App\Tests\Exception;

use App\Exception\NonExistentModeratorException;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Exception\NonExistentModeratorException
 */
class NonExistentModeratorExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $message = 'I\'m hungry';
        $exception = new NonExistentModeratorException($message);

        $this->assertSame(sprintf('Moderator does not exist. %s', $message), $exception->getMessage());
    }
}
