<?php

namespace App\Tests\Exception;

use App\Exception\NonExistentSubrodditException;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Exception\NonExistentSubrodditException
 */
class NonExistentSubrodditExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $message = 'I\'m hungry';
        $exception = new NonExistentSubrodditException($message);

        $this->assertSame(sprintf('Subroddit does not exist. %s', $message), $exception->getMessage());
    }
}
