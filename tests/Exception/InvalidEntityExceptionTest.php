<?php

namespace App\Tests\Exception;

use App\Exception\InvalidEntityException;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Exception\InvalidEntityException
 */
class InvalidEntityExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $message = 'what a time to be alive';
        $exception = new InvalidEntityException($message);

        $this->assertSame($message, $exception->getMessage());
    }
}
