<?php

namespace App\Tests\Exception;

use App\Exception\InvalidRequestException;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Exception\InvalidRequestException
 */
class InvalidRequestExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $message = 'how can this be invalid?!?!';
        $exception = new InvalidRequestException($message);

        $this->assertSame($message, $exception->getMessage());
    }
}
