<?php

namespace App\Tests\Exception;

use App\Exception\NonExistentSubscriptionException;
use App\Tests\TestCase;

/**
 * @internal
 * @covers \App\Exception\NonExistentSubscriptionException
 */
class NonExistentSubscriptionExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $message = 'I\'m hungry';
        $exception = new NonExistentSubscriptionException($message);

        $this->assertSame(sprintf('Subscription does not exist. %s', $message), $exception->getMessage());
    }
}
