<?php

namespace App\Tests\Entity;

use App\Entity\Subroddit;
use App\Entity\Subscription;
use App\Entity\User;
use App\Tests\TestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Entity\Subscription
 */
class SubscriptionTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $subscription = new Subscription($userMock, $subrodditMock);

        $result = $subscription->getSubroddit();
        $this->assertSame($subrodditMock, $result);

        $result = $subscription->getUser();
        $this->assertSame($userMock, $result);

        $result = $subscription->getFlair();
        $this->assertSame('', $result);

        $result = $subscription->getSince();
        $this->assertInstanceOf(\DateTimeInterface::class, $result);
    }

    /**
     * @test
     */
    public function getAndSetSubroddit(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $subscription = new Subscription($userMock, $subrodditMock);

        $result = $subscription->getSubroddit();
        $this->assertSame($subrodditMock, $result);

        $newSubrodditMock = \Mockery::mock(Subroddit::class);
        $subscription->setSubroddit($newSubrodditMock);
        $result = $subscription->getSubroddit();
        $this->assertSame($newSubrodditMock, $result);
    }

    /**
     * @test
     */
    public function getAndSetUser(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $subscription = new Subscription($userMock, $subrodditMock);

        $result = $subscription->getUser();
        $this->assertSame($userMock, $result);

        $newUserMock = \Mockery::mock(User::class);
        $subscription->setUser($newUserMock);
        $result = $subscription->getUser();
        $this->assertSame($newUserMock, $result);
    }

    /**
     * @test
     */
    public function getAndSetSince(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $subscription = new Subscription($userMock, $subrodditMock);

        $result = $subscription->getSince();
        $this->assertInstanceOf(\DateTimeInterface::class, $result);

        $since = new \DateTime();
        $subscription->setSince($since);
        $result = $subscription->getSince();
        $this->assertSame($since, $result);
    }

    /**
     * @test
     */
    public function getAndSetFlair(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $subscription = new Subscription($userMock, $subrodditMock);

        $result = $subscription->getFlair();
        $this->assertSame('', $result);

        $flair = 'my-flair';
        $subscription->setFlair($flair);
        $result = $subscription->getFlair();
        $this->assertSame($flair, $result);
    }

    /**
     * @test
     */
    public function validProperties(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);
        $subscription = new Subscription($userMock, $subrodditMock);

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        foreach (['user', 'subroddit', 'since', 'flair'] as $property) {
            $result = $validator->validateProperty($subscription, $property);
            $this->assertSame(0, $result->count());
        }
    }

    /**
     * @test
     */
    public function flairPropertyIsTooLong(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);
        $subscription = new Subscription($userMock, $subrodditMock);
        $subscription->setFlair('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($subscription, 'flair');
        $this->assertSame(1, $result->count());
        $this->assertSame('Subscription flair is too long', $result->get(0)->getMessage());
    }
}
