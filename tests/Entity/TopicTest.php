<?php

namespace App\Tests\Entity;

use App\Entity\Subroddit;
use App\Entity\Topic;
use App\Entity\User;
use App\Tests\TestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Entity\Topic
 */
class TopicTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getCreated();
        $this->assertInstanceOf(\DateTime::class, $result);

        $result = $topic->getDescription();
        $this->assertSame('', $result);

        $result = $topic->getId();
        $this->assertSame(0, $result);

        $result = $topic->getOp();
        $this->assertSame($userMock, $result);

        $result = $topic->getSubroddit();
        $this->assertSame($subrodditMock, $result);

        $result = $topic->getTitle();
        $this->assertSame('', $result);
    }

    /**
     * @test
     */
    public function getAndSetId(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getId();
        $this->assertSame(0, $result);

        $id = 1984;
        $topic->setId($id);
        $result = $topic->getId();
        $this->assertSame($id, $result);
    }

    /**
     * @test
     */
    public function getAndSetOp(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getOp();
        $this->assertSame($userMock, $result);

        $newUserMock = \Mockery::mock(User::class);
        $topic->setOp($newUserMock);
        $result = $topic->getOp();
        $this->assertSame($newUserMock, $result);
    }

    /**
     * @test
     */
    public function getAndSetSubroddit(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getSubroddit();
        $this->assertSame($subrodditMock, $result);

        $newSubrodditMock = \Mockery::mock(Subroddit::class);
        $topic->setSubroddit($newSubrodditMock);
        $result = $topic->getSubroddit();
        $this->assertSame($newSubrodditMock, $result);
    }

    /**
     * @test
     */
    public function getAndSetTitle(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getTitle();
        $this->assertSame('', $result);

        $title = 'my-title';
        $topic->setTitle($title);
        $result = $topic->getTitle();
        $this->assertSame($title, $result);
    }

    /**
     * @test
     */
    public function getAndSetDescription(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getDescription();
        $this->assertSame('', $result);

        $description = 'my-description';
        $topic->setDescription($description);
        $result = $topic->getDescription();
        $this->assertSame($description, $result);
    }

    /**
     * @test
     */
    public function getAndSetCreated(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $topic = new Topic($userMock, $subrodditMock);

        $result = $topic->getCreated();
        $this->assertInstanceOf(\DateTime::class, $result);

        $created = new \DateTime();
        $topic->setCreated($created);
        $result = $topic->getCreated();
        $this->assertSame($created, $result);
    }

    /**
     * @test
     */
    public function validProperties(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);
        $topic = new Topic($userMock, $subrodditMock);

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        foreach (['created', 'description', 'id', 'op', 'subroddit', 'title'] as $property) {
            $result = $validator->validateProperty($topic, $property);
            $this->assertSame(0, $result->count());
        }
    }

    /**
     * @test
     */
    public function titlePropertyIsTooLong(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);
        $topic = new Topic($userMock, $subrodditMock);
        $topic->setTitle('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($topic, 'title');
        $this->assertSame(1, $result->count());
        $this->assertSame('Topic title is too long', $result->get(0)->getMessage());
    }

    /**
     * @test
     */
    public function descriptionPropertyIsTooLong(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);
        $topic = new Topic($userMock, $subrodditMock);
        $topic->setDescription('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($topic, 'description');
        $this->assertSame(1, $result->count());
        $this->assertSame('Topic description is too long', $result->get(0)->getMessage());
    }
}
