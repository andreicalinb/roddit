<?php

namespace App\Tests\Entity;

use App\Entity\Moderator;
use App\Entity\Subscription;
use App\Entity\Topic;
use App\Entity\User;
use App\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Entity\User
 */
class UserTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $user = new User();

        $result = $user->getEmail();
        $this->assertSame('', $result);

        $result = $user->getId();
        $this->assertSame(0, $result);

        $result = $user->getIsActive();
        $this->assertTrue($result);

        $result = $user->getPassword();
        $this->assertSame('', $result);

        $result = $user->getUsername();
        $this->assertSame('', $result);

        $result = $user->getModerators();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertEmpty($result);

        $result = $user->getSubscriptions();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertEmpty($result);

        $result = $user->getTopics();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertEmpty($result);
    }

    /**
     * @test
     */
    public function getAndSetId(): void
    {
        $user = new User();

        $result = $user->getId();
        $this->assertSame(0, $result);

        $id = 1984;
        $user->setId($id);
        $result = $user->getId();
        $this->assertSame($id, $result);
    }

    /**
     * @test
     */
    public function getAndSetUsername(): void
    {
        $user = new User();

        $result = $user->getUsername();
        $this->assertSame('', $result);

        $username = 'my-username';
        $user->setUsername($username);
        $result = $user->getUsername();
        $this->assertSame($username, $result);
    }

    /**
     * @test
     */
    public function getAndSetPassword(): void
    {
        $user = new User();

        $result = $user->getPassword();
        $this->assertSame('', $result);

        $password = 'my-password';
        $user->setPassword($password);
        $result = $user->getPassword();
        $this->assertSame($password, $result);
    }

    /**
     * @test
     */
    public function getAndSetEmail(): void
    {
        $user = new User();

        $result = $user->getEmail();
        $this->assertSame('', $result);

        $email = 'my@email.com';
        $user->setEmail($email);
        $result = $user->getEmail();
        $this->assertSame($email, $result);
    }

    /**
     * @test
     */
    public function getAndSetCreated(): void
    {
        $user = new User();

        $result = $user->getCreated();
        $this->assertInstanceOf(\DateTimeInterface::class, $result);

        $created = new \DateTime();
        $user->setCreated($created);
        $result = $user->getCreated();
        $this->assertSame($created, $result);
    }

    /**
     * @test
     */
    public function getAndSetModified(): void
    {
        $user = new User();

        $result = $user->getModified();
        $this->assertInstanceOf(\DateTimeInterface::class, $result);

        $modified = new \DateTime();
        $user->setModified($modified);
        $result = $user->getModified();
        $this->assertSame($modified, $result);
    }

    /**
     * @test
     */
    public function getAndSetIsActive(): void
    {
        $user = new User();

        $result = $user->getIsActive();
        $this->assertTrue($result);

        $isActive = false;
        $user->setIsActive($isActive);
        $result = $user->getIsActive();
        $this->assertSame($isActive, $result);
    }

    /**
     * @test
     */
    public function getRoles(): void
    {
        $user = new User();

        $result = $user->getRoles();
        $this->assertSame([], $result);
    }

    /**
     * @test
     */
    public function getSalt(): void
    {
        $user = new User();

        $result = $user->getSalt();
        $this->assertSame('', $result);
    }

    /**
     * @test
     */
    public function eraseCredentials(): void
    {
        $user = new User();

        $user->eraseCredentials();
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function addAndRemoveModerators(): void
    {
        $user = new User();

        $moderatorMock = \Mockery::mock(Moderator::class);
        $moderatorMock->shouldReceive('setUser')->once();

        $anotherModeratorMock = \Mockery::mock(Moderator::class);
        $anotherModeratorMock->shouldReceive('setUser')->once();

        $user->addModerator($moderatorMock);
        $user->addModerator($anotherModeratorMock);
        $result = $user->getModerators();
        $this->assertCount(2, $result->getValues());

        $user->addModerator($moderatorMock);
        $result = $user->getModerators();
        $this->assertCount(2, $result->getValues());

        $user->removeModerator($moderatorMock);
        $result = $user->getModerators();
        $this->assertCount(1, $result->getValues());

        $user->removeModerator($moderatorMock);
        $result = $user->getModerators();
        $this->assertCount(1, $result->getValues());
    }

    /**
     * @test
     */
    public function addAndRemoveSubscription(): void
    {
        $user = new User();

        $subscriptionMock = \Mockery::mock(Subscription::class);
        $subscriptionMock->shouldReceive('setUser')->once();

        $anotherSubscriptionMock = \Mockery::mock(Subscription::class);
        $anotherSubscriptionMock->shouldReceive('setUser')->once();

        $user->addSubscription($subscriptionMock);
        $user->addSubscription($anotherSubscriptionMock);
        $result = $user->getSubscriptions();
        $this->assertCount(2, $result->getValues());

        $user->addSubscription($subscriptionMock);
        $result = $user->getSubscriptions();
        $this->assertCount(2, $result->getValues());

        $user->removeSubscription($subscriptionMock);
        $result = $user->getSubscriptions();
        $this->assertCount(1, $result->getValues());

        $user->removeSubscription($subscriptionMock);
        $result = $user->getSubscriptions();
        $this->assertCount(1, $result->getValues());
    }

    /**
     * @test
     */
    public function addAndRemoveTopic(): void
    {
        $user = new User();

        $topicMock = \Mockery::mock(Topic::class);
        $topicMock->shouldReceive('setOp')->once();

        $anotherTopicMock = \Mockery::mock(Topic::class);
        $anotherTopicMock->shouldReceive('setOp')->once();

        $user->addTopic($topicMock);
        $user->addTopic($anotherTopicMock);
        $result = $user->getTopics();
        $this->assertCount(2, $result->getValues());

        $user->addTopic($topicMock);
        $result = $user->getTopics();
        $this->assertCount(2, $result->getValues());

        $user->removeTopic($topicMock);
        $result = $user->getTopics();
        $this->assertCount(1, $result->getValues());

        $user->removeTopic($topicMock);
        $result = $user->getTopics();
        $this->assertCount(1, $result->getValues());
    }

    /**
     * @test
     */
    public function validProperties(): void
    {
        $user = new User();

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $properties = [
            'created',
            'email',
            'id',
            'isActive',
            'modified',
            'password',
            'username',
            'moderators',
            'subscriptions',
            'topics',
        ];
        foreach ($properties as $property) {
            $result = $validator->validateProperty($user, $property);
            $this->assertSame(0, $result->count());
        }
    }

    /**
     * @test
     */
    public function usernamePropertyIsTooLong(): void
    {
        $user = new User();
        $user->setUsername('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($user, 'username');
        $this->assertSame(1, $result->count());
        $this->assertSame('User username is too long', $result->get(0)->getMessage());
    }

    /**
     * @test
     */
    public function passwordPropertyIsTooLong(): void
    {
        $user = new User();
        $user->setPassword('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($user, 'password');
        $this->assertSame(1, $result->count());
        $this->assertSame('User password is too long', $result->get(0)->getMessage());
    }

    /**
     * @test
     */
    public function emailPropertyIsTooLong(): void
    {
        $user = new User();
        $user->setEmail('aaaaaaaaaaaaaaaaaaaaaa@aaaa.com');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($user, 'email');
        $this->assertSame(1, $result->count());
        $this->assertSame('User email is too long', $result->get(0)->getMessage());
    }

    /**
     * @test
     */
    public function emailPropertyHasAnInvalidFormat(): void
    {
        $user = new User();
        $user->setEmail('aaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($user, 'email');
        $this->assertSame(1, $result->count());
        $this->assertSame('Invalid user email format', $result->get(0)->getMessage());
    }
}
