<?php

namespace App\Tests\Entity;

use App\Entity\Moderator;
use App\Entity\Subroddit;
use App\Entity\User;
use App\Tests\TestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Entity\Moderator
 */
class ModeratorTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $moderator = new Moderator($userMock, $subrodditMock);

        $result = $moderator->getSubroddit();
        $this->assertSame($subrodditMock, $result);

        $result = $moderator->getUser();
        $this->assertSame($userMock, $result);

        $result = $moderator->getSince();
        $this->assertInstanceOf(\DateTimeInterface::class, $result);
    }

    /**
     * @test
     */
    public function getAndSetSubroddit(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $moderator = new Moderator($userMock, $subrodditMock);

        $result = $moderator->getSubroddit();
        $this->assertSame($subrodditMock, $result);

        $newSubrodditMock = \Mockery::mock(Subroddit::class);
        $moderator->setSubroddit($newSubrodditMock);
        $result = $moderator->getSubroddit();
        $this->assertSame($newSubrodditMock, $result);
    }

    /**
     * @test
     */
    public function getAndSetUser(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $moderator = new Moderator($userMock, $subrodditMock);

        $result = $moderator->getUser();
        $this->assertSame($userMock, $result);

        $newUserMock = \Mockery::mock(User::class);
        $moderator->setUser($newUserMock);
        $result = $moderator->getUser();
        $this->assertSame($newUserMock, $result);
    }

    /**
     * @test
     */
    public function getAndSetSince(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);

        $moderator = new Moderator($userMock, $subrodditMock);

        $result = $moderator->getSince();
        $this->assertInstanceOf(\DateTimeInterface::class, $result);

        $since = new \DateTime();
        $moderator->setSince($since);
        $result = $moderator->getSince();
        $this->assertSame($since, $result);
    }

    /**
     * @test
     */
    public function validProperties(): void
    {
        $subrodditMock = \Mockery::mock(Subroddit::class);
        $userMock = \Mockery::mock(User::class);
        $moderator = new Moderator($userMock, $subrodditMock);

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        foreach (['since', 'subroddit', 'user'] as $property) {
            $result = $validator->validateProperty($moderator, $property);
            $this->assertSame(0, $result->count());
        }
    }
}
