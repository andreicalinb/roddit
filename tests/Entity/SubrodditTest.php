<?php

namespace App\Tests\Entity;

use App\Entity\Moderator;
use App\Entity\Subroddit;
use App\Entity\Subscription;
use App\Entity\Topic;
use App\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 * @covers \App\Entity\Subroddit
 */
class SubrodditTest extends TestCase
{
    /**
     * @test
     */
    public function constructor(): void
    {
        $subroddit = new Subroddit();

        $result = $subroddit->getId();
        $this->assertSame(0, $result);

        $result = $subroddit->getLogo();
        $this->assertSame('', $result);

        $result = $subroddit->getName();
        $this->assertSame('', $result);

        $result = $subroddit->getShortDesc();
        $this->assertSame('', $result);

        $result = $subroddit->getModerators();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertEmpty($result);

        $result = $subroddit->getSubscriptions();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertEmpty($result);

        $result = $subroddit->getTopics();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertEmpty($result);
    }

    /**
     * @test
     */
    public function getAndSetId(): void
    {
        $subroddit = new Subroddit();

        $result = $subroddit->getId();
        $this->assertSame(0, $result);

        $id = 1984;
        $subroddit->setId($id);
        $result = $subroddit->getId();
        $this->assertSame($id, $result);
    }

    /**
     * @test
     */
    public function getAndSetName(): void
    {
        $subroddit = new Subroddit();

        $result = $subroddit->getName();
        $this->assertSame('', $result);

        $name = 'my-name';
        $subroddit->setName($name);
        $result = $subroddit->getName();
        $this->assertSame($name, $result);
    }

    /**
     * @test
     */
    public function getAndSetShortDesc(): void
    {
        $subroddit = new Subroddit();

        $result = $subroddit->getShortDesc();
        $this->assertSame('', $result);

        $shortDesc = 'my-short-desc';
        $subroddit->setShortDesc($shortDesc);
        $result = $subroddit->getShortDesc();
        $this->assertSame($shortDesc, $result);
    }

    /**
     * @test
     */
    public function getAndSetLogo(): void
    {
        $subroddit = new Subroddit();

        $result = $subroddit->getLogo();
        $this->assertSame('', $result);

        $shortDesc = 'my-logo';
        $subroddit->setLogo($shortDesc);
        $result = $subroddit->getLogo();
        $this->assertSame($shortDesc, $result);
    }

    /**
     * @test
     */
    public function addAndRemoveModerators(): void
    {
        $subroddit = new Subroddit();

        $moderatorMock = \Mockery::mock(Moderator::class);
        $moderatorMock->shouldReceive('setSubroddit')->once();

        $anotherModeratorMock = \Mockery::mock(Moderator::class);
        $anotherModeratorMock->shouldReceive('setSubroddit')->once();

        $subroddit->addModerator($moderatorMock);
        $subroddit->addModerator($anotherModeratorMock);
        $result = $subroddit->getModerators();
        $this->assertCount(2, $result->getValues());

        $subroddit->addModerator($moderatorMock);
        $result = $subroddit->getModerators();
        $this->assertCount(2, $result->getValues());

        $subroddit->removeModerator($moderatorMock);
        $result = $subroddit->getModerators();
        $this->assertCount(1, $result->getValues());

        $subroddit->removeModerator($moderatorMock);
        $result = $subroddit->getModerators();
        $this->assertCount(1, $result->getValues());
    }

    /**
     * @test
     */
    public function addAndRemoveSubscription(): void
    {
        $subroddit = new Subroddit();

        $subscriptionMock = \Mockery::mock(Subscription::class);
        $subscriptionMock->shouldReceive('setSubroddit')->once();

        $anotherSubscriptionMock = \Mockery::mock(Subscription::class);
        $anotherSubscriptionMock->shouldReceive('setSubroddit')->once();

        $subroddit->addSubscription($subscriptionMock);
        $subroddit->addSubscription($anotherSubscriptionMock);
        $result = $subroddit->getSubscriptions();
        $this->assertCount(2, $result->getValues());

        $subroddit->addSubscription($subscriptionMock);
        $result = $subroddit->getSubscriptions();
        $this->assertCount(2, $result->getValues());

        $subroddit->removeSubscription($subscriptionMock);
        $result = $subroddit->getSubscriptions();
        $this->assertCount(1, $result->getValues());

        $subroddit->removeSubscription($subscriptionMock);
        $result = $subroddit->getSubscriptions();
        $this->assertCount(1, $result->getValues());
    }

    /**
     * @test
     */
    public function addAndRemoveTopic(): void
    {
        $subroddit = new Subroddit();

        $topicMock = \Mockery::mock(Topic::class);
        $topicMock->shouldReceive('setSubroddit')->once();

        $anotherTopicMock = \Mockery::mock(Topic::class);
        $anotherTopicMock->shouldReceive('setSubroddit')->once();

        $subroddit->addTopic($topicMock);
        $subroddit->addTopic($anotherTopicMock);
        $result = $subroddit->getTopics();
        $this->assertCount(2, $result->getValues());

        $subroddit->addTopic($topicMock);
        $result = $subroddit->getTopics();
        $this->assertCount(2, $result->getValues());

        $subroddit->removeTopic($topicMock);
        $result = $subroddit->getTopics();
        $this->assertCount(1, $result->getValues());

        $subroddit->removeTopic($topicMock);
        $result = $subroddit->getTopics();
        $this->assertCount(1, $result->getValues());
    }

    /**
     * @test
     */
    public function validProperties(): void
    {
        $subroddit = new Subroddit();

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        foreach (['id', 'name', 'logo', 'op', 'shortDesc', 'subscriptions', 'topics'] as $property) {
            $result = $validator->validateProperty($subroddit, $property);
            $this->assertSame(0, $result->count());
        }
    }

    /**
     * @test
     */
    public function namePropertyIsTooLong(): void
    {
        $subroddit = new Subroddit();
        $subroddit->setName('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($subroddit, 'name');
        $this->assertSame(1, $result->count());
        $this->assertSame('Subroddit name is too long', $result->get(0)->getMessage());
    }

    /**
     * @test
     */
    public function shortDescPropertyIsTooLong(): void
    {
        $subroddit = new Subroddit();
        $subroddit->setShortDesc('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($subroddit, 'shortDesc');
        $this->assertSame(1, $result->count());
        $this->assertSame('Subroddit short description is too long', $result->get(0)->getMessage());
    }

    /**
     * @test
     */
    public function logoPropertyIsTooLong(): void
    {
        $subroddit = new Subroddit();
        $subroddit->setLogo('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

        /** @var ValidatorInterface $validator */
        $validator = self::bootKernel()->getContainer()->get('validator');

        $result = $validator->validateProperty($subroddit, 'logo');
        $this->assertSame(1, $result->count());
        $this->assertSame('Subroddit logo is too long', $result->get(0)->getMessage());
    }
}
