<?php

namespace App\Tests\Controller;

use App\Controller\AbstractController;
use App\Controller\UserController;
use App\Entity\User;
use App\Monolog\Logger;
use App\Repository\UserRepository;
use App\Service\UserService;
use App\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 * @covers \App\Controller\AbstractController
 * @covers \App\Controller\UserController
 */
class UserControllerTest extends TestCase
{
    /**
     * @test
     */
    public function getUserByEmailSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $email = 'setUpShop@jamaica.com';
        $queryParams = ['QUERY_STRING' => sprintf('email=%s', $email)];
        $request = new Request([], [], [], [], [], $queryParams);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['email' => $email])
            ->andReturn($user);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($user, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => User::GROUPS_GET])
            ->andReturn([]);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->getUserByEmail($request, $userRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function getUserByEmailWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $email = 'setUpShop@jamaica.com';
        $queryParams = ['QUERY_STRING' => sprintf('email=%s', $email)];
        $request = new Request([], [], [], [], [], $queryParams);

        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['email' => $email])
            ->andThrow(new $exceptionClass(''));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->getUserByEmail($request, $userRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }

    /**
     * @test
     */
    public function getUserByIdSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $userId = 532;
        $attributes = ['userId' => $userId];
        $request = new Request([], [], $attributes);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['id' => $userId])
            ->andReturn($user);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($user, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => User::GROUPS_GET])
            ->andReturn([]);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->getUserById($request, $userRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function getUserByIdWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $userId = 532;
        $attributes = ['userId' => $userId];
        $request = new Request([], [], $attributes);

        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['id' => $userId])
            ->andThrow(new $exceptionClass(''));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->getUserById($request, $userRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }

    /**
     * @test
     */
    public function registerSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $user = new User();
        $userServiceMock = \Mockery::mock(UserService::class);
        $userServiceMock->shouldReceive('create')
            ->once()
            ->with($content)
            ->andReturn($user);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($user, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => User::GROUPS_GET])
            ->andReturn([]);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->register($request, $userServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_CREATED, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"User successfully registered"}', $responseContent);
    }

    /**
     * @test
     */
    public function registerWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $userServiceMock = \Mockery::mock(UserService::class);
        $userServiceMock->shouldReceive('create')
            ->once()
            ->with($content)
            ->andThrow(new \Exception());

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->register($request, $userServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function updateUserByIdSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $userId = 532;
        $attributes = ['userId' => $userId];
        $content = ['b' => 'la'];
        $request = new Request([], [], $attributes, [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $content['id'] = $userId;
        $user = new User();
        $userServiceMock = \Mockery::mock(UserService::class);
        $userServiceMock->shouldReceive('update')
            ->once()
            ->with($content)
            ->andReturn($user);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($user, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => User::GROUPS_GET])
            ->andReturn([]);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->updateUserById($request, $userServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function updateUserByIdWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $userId = 532;
        $attributes = ['userId' => $userId];
        $content = ['b' => 'la'];
        $request = new Request([], [], $attributes, [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $content['id'] = $userId;
        $userServiceMock = \Mockery::mock(UserService::class);
        $userServiceMock->shouldReceive('update')
            ->once()
            ->with($content)
            ->andThrow(new $exceptionClass(''));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->updateUserById($request, $userServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }

    /**
     * @test
     */
    public function getUserByUsernameSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $username = 'SbxUser';
        $queryParams = ['QUERY_STRING' => sprintf('username=%s', $username)];
        $request = new Request([], [], [], [], [], $queryParams);

        $user = new User();
        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['username' => $username])
            ->andReturn($user);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($user, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => User::GROUPS_GET])
            ->andReturn([]);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->getUserByUsername($request, $userRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function getUserByUsernameWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $username = 'SbxUser';
        $queryParams = ['QUERY_STRING' => sprintf('username=%s', $username)];
        $request = new Request([], [], [], [], [], $queryParams);

        $userRepositoryMock = \Mockery::mock(UserRepository::class);
        $userRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['username' => $username])
            ->andThrow(new $exceptionClass(''));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new UserController($loggerMock, $serializerMock);

        $response = $controller->getUserByUsername($request, $userRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }
}
