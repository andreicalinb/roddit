<?php

namespace App\Tests\Controller;

use App\Controller\AbstractController;
use App\Controller\TopicController;
use App\Entity\Topic;
use App\Monolog\Logger;
use App\Repository\TopicRepository;
use App\Service\TopicService;
use App\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 * @covers \App\Controller\AbstractController
 * @covers \App\Controller\TopicController
 */
class TopicControllerTest extends TestCase
{
    /**
     * @test
     */
    public function createSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $topicServiceMock = \Mockery::mock(TopicService::class);
        $topicServiceMock->shouldReceive('create')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new TopicController($loggerMock, $serializerMock);

        $response = $controller->create($request, $topicServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_CREATED, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Topic successfully created"}', $responseContent);
    }

    /**
     * @test
     */
    public function createWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $topicServiceMock = \Mockery::mock(TopicService::class);
        $topicServiceMock->shouldReceive('create')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('I cry'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new TopicController($loggerMock, $serializerMock);

        $response = $controller->create($request, $topicServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function getTopicsBySubrodditAndUserSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $subrodditId = 3371;
        $userId = 1373;
        $routeParams = ['_route_params' => ['subrodditId' => $subrodditId, 'userId' => $userId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $topicRepositoryMock = \Mockery::mock(TopicRepository::class);
        $topicRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['subroddit' => $subrodditId, 'op' => $userId], [], $itemsPerPage, 0)
            ->andReturn([]);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Topic::GROUPS_GET])
            ->andReturn([]);

        $controller = new TopicController($loggerMock, $serializerMock);

        $response = $controller->getTopicsBySubrodditAndUser($request, $topicRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     */
    public function getTopicsBySubrodditAndUserWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $subrodditId = 3371;
        $userId = 1373;
        $routeParams = ['_route_params' => ['subrodditId' => $subrodditId, 'userId' => $userId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $topicRepositoryMock = \Mockery::mock(TopicRepository::class);
        $topicRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['subrodditId' => $subrodditId, 'opId' => $userId], [], $itemsPerPage, 0)
            ->andReturn([]);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Topic::GROUPS_GET])
            ->andThrow(new \Exception('I cry'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new TopicController($loggerMock, $serializerMock);

        $response = $controller->getTopicsBySubrodditAndUser($request, $topicRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function getTopicsByUserSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $userId = 1373;
        $routeParams = ['_route_params' => ['userId' => $userId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $topicRepositoryMock = \Mockery::mock(TopicRepository::class);
        $topicRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['op' => $userId], [], $itemsPerPage, 0)
            ->andReturn([]);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Topic::GROUPS_GET])
            ->andReturn([]);

        $controller = new TopicController($loggerMock, $serializerMock);

        $response = $controller->getTopicsByUser($request, $topicRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     */
    public function getTopicsByUserWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $userId = 1373;
        $routeParams = ['_route_params' => ['userId' => $userId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $topicRepositoryMock = \Mockery::mock(TopicRepository::class);
        $topicRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['op' => $userId], [], $itemsPerPage, 0)
            ->andReturn([]);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Topic::GROUPS_GET])
            ->andThrow(new \Exception('I cry'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new TopicController($loggerMock, $serializerMock);

        $response = $controller->getTopicsByUser($request, $topicRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }
}
