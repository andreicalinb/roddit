<?php

namespace App\Tests\Controller;

use App\Controller\AbstractController;
use App\Controller\SubscriptionController;
use App\Entity\Subscription;
use App\Monolog\Logger;
use App\Repository\SubscriptionRepository;
use App\Service\SubscriptionService;
use App\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 * @covers \App\Controller\AbstractController
 * @covers \App\Controller\SubscriptionController
 */
class SubscriptionControllerTest extends TestCase
{
    /**
     * @test
     */
    public function updateFlairBySubscriptionSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subscriptionServiceMock = \Mockery::mock(SubscriptionService::class);
        $subscriptionServiceMock->shouldReceive('updateFlair')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->updateFlairBySubscription($request, $subscriptionServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Flair successfully updated"}', $responseContent);
    }

    /**
     * @test
     */
    public function updateFlairBySubscriptionWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subscriptionServiceMock = \Mockery::mock(SubscriptionService::class);
        $subscriptionServiceMock->shouldReceive('updateFlair')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('I cannot believe this'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->updateFlairBySubscription($request, $subscriptionServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function subscribeSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subscriptionServiceMock = \Mockery::mock(SubscriptionService::class);
        $subscriptionServiceMock->shouldReceive('subscribe')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->subscribe($request, $subscriptionServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"User successfully subscribed to subroddit"}', $responseContent);
    }

    /**
     * @test
     */
    public function subscribeWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subscriptionServiceMock = \Mockery::mock(SubscriptionService::class);
        $subscriptionServiceMock->shouldReceive('subscribe')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('I cannot believe this'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->subscribe($request, $subscriptionServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function unsubscribeSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subscriptionServiceMock = \Mockery::mock(SubscriptionService::class);
        $subscriptionServiceMock->shouldReceive('unsubscribe')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->unsubscribe($request, $subscriptionServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"User successfully unsubscribed from subroddit"}', $responseContent);
    }

    /**
     * @test
     */
    public function unsubscribeWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subscriptionServiceMock = \Mockery::mock(SubscriptionService::class);
        $subscriptionServiceMock->shouldReceive('unsubscribe')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('I cannot believe this'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->unsubscribe($request, $subscriptionServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function getSubscriptionsByUserSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $userId = 1373;
        $routeParams = ['_route_params' => ['userId' => $userId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);
        $subscriptionRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['user' => $userId], [], $itemsPerPage, 0);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Subscription::GROUPS_GET])
            ->andReturn([]);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->getSubscriptionsByUser($request, $subscriptionRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     */
    public function getSubscriptionsByUserWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $userId = 1373;
        $routeParams = ['_route_params' => ['userId' => $userId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $subscriptionRepositoryMock = \Mockery::mock(SubscriptionRepository::class);
        $subscriptionRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['userId' => $userId], [], $itemsPerPage, 0)
            ->andThrow(new \Exception());

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubscriptionController($loggerMock, $serializerMock);

        $response = $controller->getSubscriptionsByUser($request, $subscriptionRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }
}
