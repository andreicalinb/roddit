<?php

namespace App\Tests\Controller;

use App\Controller\AbstractController;
use App\Controller\ModeratorController;
use App\Entity\Moderator;
use App\Monolog\Logger;
use App\Repository\ModeratorRepository;
use App\Service\ModeratorService;
use App\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 * @covers \App\Controller\AbstractController
 * @covers \App\Controller\ModeratorController
 */
class ModeratorControllerTest extends TestCase
{
    /**
     * @test
     */
    public function registerSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $moderatorServiceMock = \Mockery::mock(ModeratorService::class);
        $moderatorServiceMock->shouldReceive('register')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new ModeratorController($loggerMock, $serializerMock);

        $response = $controller->register($request, $moderatorServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Moderator successfully registered"}', $responseContent);
    }

    /**
     * @test
     */
    public function registerWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $moderatorServiceMock = \Mockery::mock(ModeratorService::class);
        $moderatorServiceMock->shouldReceive('register')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('I cannot believe this'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new ModeratorController($loggerMock, $serializerMock);

        $response = $controller->register($request, $moderatorServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function getModeratorsBySubrodditSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $subrodditId = 563463;
        $routeParams = ['_route_params' => ['subrodditId' => $subrodditId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $moderatorRepositoryMock = \Mockery::mock(ModeratorRepository::class);
        $moderatorRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['subroddit' => $subrodditId], [], $itemsPerPage, 0)
            ->andReturn([]);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Moderator::GROUPS_GET])
            ->andReturn([]);

        $controller = new ModeratorController($loggerMock, $serializerMock);

        $response = $controller->getModeratorsBySubroddit($request, $moderatorRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     */
    public function getModeratorsBySubrodditWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $subrodditId = 563463;
        $routeParams = ['_route_params' => ['subrodditId' => $subrodditId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $moderatorRepositoryMock = \Mockery::mock(ModeratorRepository::class);
        $moderatorRepositoryMock->shouldReceive('findBy')
            ->once()
            ->with(['subrodditId' => $subrodditId], [], $itemsPerPage, 0)
            ->andThrow(new \Exception('Blanisimo'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new ModeratorController($loggerMock, $serializerMock);

        $response = $controller->getModeratorsBySubroddit($request, $moderatorRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function unregisterSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $moderatorServiceMock = \Mockery::mock(ModeratorService::class);
        $moderatorServiceMock->shouldReceive('unregister')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new ModeratorController($loggerMock, $serializerMock);

        $response = $controller->unregister($request, $moderatorServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Moderator successfully unregistered"}', $responseContent);
    }

    /**
     * @test
     */
    public function unregisterWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['b' => 'la'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $moderatorServiceMock = \Mockery::mock(ModeratorService::class);
        $moderatorServiceMock->shouldReceive('unregister')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('I cannot believe this'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new ModeratorController($loggerMock, $serializerMock);

        $response = $controller->unregister($request, $moderatorServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }
}
