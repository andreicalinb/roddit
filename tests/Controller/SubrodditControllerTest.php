<?php

namespace App\Tests\Controller;

use App\Controller\AbstractController;
use App\Controller\SubrodditController;
use App\Entity\Subroddit;
use App\Monolog\Logger;
use App\Repository\SubrodditRepository;
use App\Service\SubrodditService;
use App\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 * @covers \App\Controller\AbstractController
 * @covers \App\Controller\SubrodditController
 */
class SubrodditControllerTest extends TestCase
{
    /**
     * @test
     */
    public function createSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $content = ['man' => 'yes?'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subrodditServiceMock = \Mockery::mock(SubrodditService::class);
        $subrodditServiceMock->shouldReceive('create')->once()->with($content);

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->create($request, $subrodditServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_CREATED, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Subroddit successfully created"}', $responseContent);
    }

    /**
     * @test
     */
    public function createWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $content = ['man' => 'yes?'];
        $request = new Request([], [], [], [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $subrodditServiceMock = \Mockery::mock(SubrodditService::class);
        $subrodditServiceMock->shouldReceive('create')
            ->once()
            ->with($content)
            ->andThrow(new \Exception('say what?'));

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->create($request, $subrodditServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function getSubrodditByIdSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $subrodditId = 777;
        $attributes = ['subrodditId' => $subrodditId];
        $request = new Request([], [], $attributes);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['id' => $subrodditId])
            ->andReturn($subroddit);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($subroddit, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET])
            ->andReturn([]);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->getSubrodditById($request, $subrodditRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function getSubrodditByIdWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $subrodditId = 777;
        $attributes = ['subrodditId' => $subrodditId];
        $request = new Request([], [], $attributes);

        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['id' => $subrodditId])
            ->andThrow(new $exceptionClass());

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->getSubrodditById($request, $subrodditRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }

    /**
     * @test
     */
    public function getSubrodditsByModeratorSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $moderatorId = 35423;
        $routeParams = ['_route_params' => ['moderatorId' => $moderatorId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findByModeratorId')
            ->once()
            ->with($moderatorId, $page, $itemsPerPage)
            ->andReturn([]);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with([], JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET])
            ->andReturn([]);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->getSubrodditsByModerator($request, $subrodditRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     */
    public function getSubrodditsByModeratorWithException(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $itemsPerPage = 8;
        $page = 1;
        $queryParams = ['QUERY_STRING' => sprintf('itemsPerPage=%d&page=%d', $itemsPerPage, $page)];
        $moderatorId = 35423;
        $routeParams = ['_route_params' => ['moderatorId' => $moderatorId]];
        $request = new Request([], [], $routeParams, [], [], $queryParams);

        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findByModeratorId')
            ->once()
            ->with($moderatorId, $page, $itemsPerPage)
            ->andThrow(new \Exception());

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->getSubrodditsByModerator($request, $subrodditRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":"Bad request"}', $responseContent);
    }

    /**
     * @test
     */
    public function getSubrodditByNameSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $name = 'Romania';
        $queryParams = ['QUERY_STRING' => sprintf('name=%s', $name)];
        $request = new Request([], [], [], [], [], $queryParams);

        $subroddit = new Subroddit();
        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['name' => $name])
            ->andReturn($subroddit);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($subroddit, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET])
            ->andReturn([]);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->getSubrodditByName($request, $subrodditRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function getSubrodditByNameWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $name = 'Romania';
        $queryParams = ['QUERY_STRING' => sprintf('name=%s', $name)];
        $request = new Request([], [], [], [], [], $queryParams);

        $subrodditRepositoryMock = \Mockery::mock(SubrodditRepository::class);
        $subrodditRepositoryMock->shouldReceive('findOneBy')
            ->once()
            ->with(['name' => $name])
            ->andThrow(new $exceptionClass());

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->getSubrodditByName($request, $subrodditRepositoryMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }

    /**
     * @test
     */
    public function updateSubrodditByIdSuccessfully(): void
    {
        $loggerMock = \Mockery::mock(Logger::class);

        $subrodditId = 321;
        $attributes = ['subrodditId' => $subrodditId];
        $content = ['bbb' => 'lllaaa'];
        $request = new Request([], [], $attributes, [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $content['id'] = $subrodditId;
        $subroddit = new Subroddit();
        $subrodditServiceMock = \Mockery::mock(SubrodditService::class);
        $subrodditServiceMock->shouldReceive('update')
            ->once()
            ->with($content)
            ->andReturn($subroddit);

        $serializerMock = \Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')
            ->once()
            ->with($subroddit, JsonEncoder::FORMAT, [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET])
            ->andReturn([]);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->updateSubrodditById($request, $subrodditServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame('{"message":[]}', $responseContent);
    }

    /**
     * @test
     * @dataProvider yamlFile
     */
    public function updateSubrodditByIdWithException(string $exceptionClass, int $expectedResultCode, string $expectedResultContent): void
    {
        $loggerMock = \Mockery::mock(Logger::class);
        $loggerMock->shouldReceive('addError')->once();

        $subrodditId = 321;
        $attributes = ['subrodditId' => $subrodditId];
        $content = ['bbb' => 'lllaaa'];
        $request = new Request([], [], $attributes, [], [], [], json_encode($content));
        $request->headers->set('content-type', AbstractController::REQUEST_CONTENT_TYPE_APPLICATION_JSON);

        $content['id'] = $subrodditId;
        $subrodditServiceMock = \Mockery::mock(SubrodditService::class);
        $subrodditServiceMock->shouldReceive('update')
            ->once()
            ->with($content)
            ->andThrow(new $exceptionClass());

        $serializerMock = \Mockery::mock(SerializerInterface::class);

        $controller = new SubrodditController($loggerMock, $serializerMock);

        $response = $controller->updateSubrodditById($request, $subrodditServiceMock);

        $responseCode = $response->getStatusCode();
        $this->assertSame($expectedResultCode, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($expectedResultContent, $responseContent);
    }
}
