<?php

namespace App\Tests\Controller;

use App\Controller\IndexController;
use App\Tests\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * @internal
 * @covers \App\Controller\IndexController
 */
class IndexControllerTest extends TestCase
{
    /**
     * @test
     */
    public function indexSuccessfully(): void
    {
        $content = 'Blanao';
        $twigMock = \Mockery::mock(Environment::class);
        $twigMock->shouldReceive('render')->once()->andReturn($content);

        $containerMock = \Mockery::mock(ContainerInterface::class);
        $containerMock->shouldReceive('has')->once()->andReturn(false, true);
        $containerMock->shouldReceive('get')->once()->andReturn($twigMock);

        $controller = new IndexController();
        $this->setRestrictedPropertyValue($controller, 'container', $containerMock);

        $response = $controller->index();

        $responseCode = $response->getStatusCode();
        $this->assertSame(Response::HTTP_OK, $responseCode);

        $responseContent = $response->getContent();
        $this->assertSame($content, $responseContent);
    }
}
