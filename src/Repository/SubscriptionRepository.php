<?php

namespace App\Repository;

use App\Entity\Subscription;
use App\Exception\NonExistentSubscriptionException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method null|Subscription find($id, $lockMode = null, $lockVersion = null)
 * @method Subscription[]    findAll()
 * @method Subscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subscription::class);
    }

    /**
     * @throws NonExistentSubscriptionException
     */
    public function findOneBy(array $criteria, array $orderBy = null): Subscription
    {
        /** @var null|Subscription $subscription */
        $subscription = parent::findOneBy($criteria, $orderBy);

        if (null === $subscription) {
            throw new NonExistentSubscriptionException();
        }

        return $subscription;
    }
}
