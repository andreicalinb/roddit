<?php

namespace App\Repository;

use App\Entity\Moderator;
use App\Entity\Subroddit;
use App\Exception\NonExistentSubrodditException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method null|Subroddit find($id, $lockMode = null, $lockVersion = null)
 * @method Subroddit[]    findAll()
 * @method Subroddit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubrodditRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subroddit::class);
    }

    /**
     * @throws NonExistentSubrodditException
     */
    public function findOneBy(array $criteria, array $orderBy = null): Subroddit
    {
        /** @var null|Subroddit $subroddit */
        $subroddit = parent::findOneBy($criteria, $orderBy);

        if (null === $subroddit) {
            throw new NonExistentSubrodditException();
        }

        return $subroddit;
    }

    public function findByModeratorId(int $moderatorId, int $page, int $itemsPerPage): array
    {
        $query = $this->createQueryBuilder('s')
            ->innerJoin(Moderator::class, 'm', Join::WITH, 's.id = m.subroddit')
            ->where(sprintf('m.user = :userId'))
            ->setParameter('userId', $moderatorId)
            ->setFirstResult($page - 1)
            ->setMaxResults($itemsPerPage);

        return $query->getQuery()->execute();
    }
}
