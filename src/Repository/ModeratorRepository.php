<?php

namespace App\Repository;

use App\Entity\Moderator;
use App\Exception\NonExistentModeratorException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method null|Moderator find($id, $lockMode = null, $lockVersion = null)
 * @method Moderator[]    findAll()
 * @method Moderator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModeratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Moderator::class);
    }

    /**
     * @throws NonExistentModeratorException
     */
    public function findOneBy(array $criteria, array $orderBy = null): Moderator
    {
        /** @var null|Moderator $moderator */
        $moderator = parent::findOneBy($criteria, $orderBy);

        if (null === $moderator) {
            throw new NonExistentModeratorException();
        }

        return $moderator;
    }
}
