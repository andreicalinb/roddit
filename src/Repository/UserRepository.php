<?php

namespace App\Repository;

use App\Entity\User;
use App\Exception\NonExistentUserException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method null|User find($id, $lockMode = null, $lockVersion = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @throws NonExistentUserException
     */
    public function findOneBy(array $criteria, array $orderBy = null): User
    {
        /** @var null|User $user */
        $user = parent::findOneBy($criteria, $orderBy);

        if (null === $user) {
            throw new NonExistentUserException();
        }

        return $user;
    }
}
