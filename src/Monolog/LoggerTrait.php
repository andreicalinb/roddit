<?php

namespace App\Monolog;

use Monolog\Logger as MonologLogger;

/**
 * Used along the LogInterface.
 *
 * @see LogInterface
 */
trait LoggerTrait
{
    protected Logger $logger;

    protected function logInfo(array $context = []): void
    {
        $this->log(MonologLogger::INFO, $context);
    }

    /**
     * @psalm-suppress DocblockTypeContradiction
     */
    protected function logError(array $context = []): void
    {
        $this->log(MonologLogger::ERROR, $context);
    }

    /**
     * Each class that use this trait should have this constant defined in order to properly group logs and filter more
     * easily through them.
     *
     * @codeCoverageIgnore
     */
    protected function getClassConstant(): string
    {
        return static::LOGS_MESSAGE;
    }

    private function log(int $level, array $context = []): void
    {
        if (isset($context['exception'])) {
            /** @var \Exception $ex */
            $ex = $context['exception'];
            $context['exception_message'] = $ex->getMessage();
            $context['exception_code'] = $ex->getCode();
            $context['exception_type'] = \get_class($ex);
            unset($context['exception']);
        }

        switch ($level) {
            case MonologLogger::INFO:
                $this->logger->addInfo($this->getClassConstant(), $context, $this);

                break;
            case MonologLogger::ERROR:
                $this->logger->addError($this->getClassConstant(), $context, $this);

                break;
            default:
        }
    }
}
