<?php

namespace App\Monolog;

use Monolog\Logger as MonologLogger;
use Psr\Log\LoggerInterface;

class Logger
{
    private MonologLogger $logger;

    /**
     * @param MonologLogger $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param object|string $module
     */
    public function addDebug(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::DEBUG, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addInfo(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::INFO, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addNotice(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::NOTICE, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addWarning(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::WARNING, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addError(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::ERROR, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addCritical(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::CRITICAL, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addAlert(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::ALERT, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    public function addEmergency(string $message, array $context = [], $module = ''): bool
    {
        return $this->addRecord(MonologLogger::EMERGENCY, $message, $context, $module);
    }

    /**
     * @param object|string $module
     */
    private function addRecord(int $level, string $message, array $context = [], $module = ''): bool
    {
        $context['mypid'] = getmypid() ?: null;
        $context['session_id'] = session_id() ?: null;
        $module = \is_object($module) ? \get_class($module) : (string) $module;
        $context['module'] = $module;

        return $this->logger->addRecord($level, $message, $context);
    }
}
