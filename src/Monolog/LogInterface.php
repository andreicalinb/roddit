<?php

namespace App\Monolog;

/**
 * Interface used along with the LogTrait in order easily and consistently log messages.
 *
 * @see LogTrait
 */
interface LogInterface
{
    /**
     * Value used by the trait in order to consistently group logs for a command, cron, worker etc.
     */
    const LOGS_MESSAGE = '';
}
