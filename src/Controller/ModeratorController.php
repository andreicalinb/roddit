<?php

namespace App\Controller;

use App\Entity\Moderator;
use App\Repository\ModeratorRepository;
use App\Service\ModeratorService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class ModeratorController extends AbstractController
{
    const LOGS_MESSAGE = 'Moderator Controller';

    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Moderator\RegisterInput::class)
     * )
     * @SWG\Response(response=200, description="Moderator registered")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Moderator")
     * @Route("/api/moderators/register", name="moderator_register", methods={"POST"})
     */
    public function register(Request $request, ModeratorService $moderatorService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $moderatorService->register($post);

            $response = new Response(
                json_encode(['message' => 'Moderator successfully registered']),
                Response::HTTP_OK
            );
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot register moderator', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="subrodditId", in="path", required=true, type="integer")
     * @SWG\Parameter(name="page", in="query", default="1", required=true, type="integer")
     * @SWG\Parameter(name="itemsPerPage", in="query", default="8", required=true, type="integer")
     * @SWG\Response(response=200, description="Moderators returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Moderator")
     * @Route(
     *     "/api/moderators/subroddit/{subrodditId}",
     *     name="get_moderators_by_subroddit",
     *     requirements={"subrodditId": "\d+"},
     *     methods={"GET"}
     * )
     */
    public function getModeratorsBySubroddit(Request $request, ModeratorRepository $moderatorRepository): Response
    {
        $get = $this->getGetParams($request);
        $params = $this->getRouteParams($request);

        try {
            $moderators = $moderatorRepository->findBy(
                ['subroddit' => $params['subrodditId']],
                [],
                $get['itemsPerPage'],
                ($get['page'] - 1) * $get['itemsPerPage']
            );
            $serializedEntities = $this->serializer->serialize(
                $moderators,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Moderator::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntities]), Response::HTTP_OK);
        } catch (\Exception $ex) {
            $this->logError(
                ['message' => 'Cannot get moderators by subroddit', 'params' => $params, 'exception' => $ex]
            );
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Moderator\UnregisterInput::class)
     * )
     * @SWG\Response(response=200, description="Moderator unregistered")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Moderator")
     * @Route("/api/moderators/unregister", name="moderator_unregister", methods={"POST"})
     */
    public function unregister(Request $request, ModeratorService $moderatorService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $moderatorService->unregister($post);

            $response = new Response(
                json_encode(['message' => 'Moderator successfully unregistered']),
                Response::HTTP_OK
            );
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot unregister moderator', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }
}
