<?php

namespace App\Controller;

use App\Monolog\Logger;
use App\Monolog\LoggerTrait;
use App\Monolog\LogInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class AbstractController extends Controller implements LogInterface
{
    use LoggerTrait;

    const MESSAGE_BAD_REQUEST = 'Bad request';

    const REQUEST_CONTENT_TYPE_APPLICATION_JSON = 'application/json';

    protected SerializerInterface $serializer;

    public function __construct(Logger $logger, SerializerInterface $serializer)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    protected function getPostParams(Request $request): array
    {
        $contentType = $request->headers->get('content-type');
        $isJsonContentType = self::REQUEST_CONTENT_TYPE_APPLICATION_JSON === $contentType;

        return $isJsonContentType ? json_decode((string) $request->getContent(), true) : $request->request->all();
    }

    protected function getGetParams(Request $request): array
    {
        parse_str((string) $request->getQueryString(), $getParams);

        return $getParams;
    }

    protected function getRouteParams(Request $request): array
    {
        return $request->attributes->get('_route_params');
    }
}
