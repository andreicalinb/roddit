<?php

namespace App\Controller;

use App\Entity\Topic;
use App\Repository\TopicRepository;
use App\Service\TopicService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class TopicController extends AbstractController
{
    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Topic\CreateInput::class)
     * )
     * @SWG\Response(response=201, description="Topic created")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Topic")
     * @Route("/api/topics/create", name="topic_create", methods={"POST"})
     */
    public function create(Request $request, TopicService $topicService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $topicService->create($post);

            $response = new Response(json_encode(['message' => 'Topic successfully created']), Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot create topic', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="subrodditId", in="path", required=true, type="integer")
     * @SWG\Parameter(name="userId", in="path", required=true, type="integer")
     * @SWG\Parameter(name="page", in="query", default="1", required=true, type="integer")
     * @SWG\Parameter(name="itemsPerPage", in="query", default="8", required=true, type="integer")
     * @SWG\Response(response=200, description="Topics returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Topic")
     * @Route(
     *     "/api/topics/subroddit/{subrodditId}/user/{userId}",
     *     name="get_topics_by_subroddit_and_user",
     *     requirements={"subrodditId": "\d+", "userId": "\d+"},
     *     methods={"GET"}
     * )
     */
    public function getTopicsBySubrodditAndUser(Request $request, TopicRepository $topicRepository): Response
    {
        $get = $this->getGetParams($request);
        $params = $this->getRouteParams($request);

        try {
            $topics = $topicRepository->findBy(
                ['subroddit' => $params['subrodditId'], 'op' => $params['userId']],
                [],
                $get['itemsPerPage'],
                ($get['page'] - 1) * $get['itemsPerPage']
            );
            $serializedEntities = $this->serializer->serialize(
                $topics,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Topic::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntities]), Response::HTTP_OK);
        } catch (\Exception $ex) {
            $this->logError(
                ['message' => 'Cannot get topics by a subroddit and an user', 'params' => $get, 'exception' => $ex]
            );
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="userId", in="path", required=true, type="integer")
     * @SWG\Parameter(name="page", in="query", default="1", required=true, type="integer")
     * @SWG\Parameter(name="itemsPerPage", in="query", default="8", required=true, type="integer")
     * @SWG\Response(response=200, description="Topics returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Topic")
     * @Route("/api/topics/user/{userId}", name="get_topics_by_user", requirements={"userId": "\d+"}, methods={"GET"})
     */
    public function getTopicsByUser(Request $request, TopicRepository $topicRepository): Response
    {
        $get = $this->getGetParams($request);
        $params = $this->getRouteParams($request);

        try {
            $topics = $topicRepository->findBy(
                ['op' => $params['userId']],
                [],
                $get['itemsPerPage'],
                ($get['page'] - 1) * $get['itemsPerPage']
            );
            $serializedEntities = $this->serializer->serialize(
                $topics,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Topic::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntities]), Response::HTTP_OK);
        } catch (\Exception $ex) {
            $this->logError(
                ['message' => 'Cannot get topics by an user', 'params' => $get, 'exception' => $ex]
            );
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }
}
