<?php

namespace App\Controller;

use App\Entity\Subroddit;
use App\Exception\NonExistentSubrodditException;
use App\Repository\SubrodditRepository;
use App\Service\SubrodditService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class SubrodditController extends AbstractController
{
    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Subroddit\CreateInput::class)
     * )
     * @SWG\Response(response=201, description="Subroddit was created")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Subroddit")
     * @Route("/api/subroddits/create", name="subroddit_create", methods={"POST"})
     */
    public function create(Request $request, SubrodditService $subrodditService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $subrodditService->create($post);

            $response = new Response(
                json_encode(['message' => 'Subroddit successfully created']),
                Response::HTTP_CREATED
            );
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot create subroddit', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="subrodditId", in="path", required=true, type="integer")
     * @SWG\Response(response=200, description="Subroddit returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="Subroddit not found")
     * @SWG\Tag(name="Subroddit")
     * @Route(
     *     "/api/subroddits/id/{subrodditId}",
     *     name="get_subroddit_by_id",
     *     requirements={"subrodditId": "\d+"},
     *     methods={"GET"}
     * )
     */
    public function getSubrodditById(Request $request, SubrodditRepository $subrodditRepository): Response
    {
        $id = $request->get('subrodditId');

        try {
            $subroddit = $subrodditRepository->findOneBy(['id' => $id]);
            $serializedEntity = $this->serializer->serialize(
                $subroddit,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentSubrodditException $ex) {
            $this->logError(['message' => 'Cannot get subroddit by id', 'id' => $id, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot get subroddit by id', 'id' => $id, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="moderatorId", in="path", required=true, type="integer")
     * @SWG\Parameter(name="page", in="query", default="1", required=true, type="integer")
     * @SWG\Parameter(name="itemsPerPage", in="query", default="8", required=true, type="integer")
     * @SWG\Response(response=200, description="Subroddits returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Subroddit")
     * @Route(
     *     "/api/subroddits/moderator/{moderatorId}",
     *     name="get_subroddits_for_moderator",
     *     requirements={"moderatorId": "\d+"},
     *     methods={"GET"}
     * )
     */
    public function getSubrodditsByModerator(Request $request, SubrodditRepository $subrodditRepository): Response
    {
        $get = $this->getGetParams($request);
        $params = $this->getRouteParams($request);

        try {
            $subroddits = $subrodditRepository->findByModeratorId(
                (int) $params['moderatorId'],
                (int) $get['page'],
                (int) $get['itemsPerPage']
            );
            $serializedEntities = $this->serializer->serialize(
                $subroddits,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntities]), Response::HTTP_OK);
        } catch (\Exception $ex) {
            $this->logError(
                ['message' => 'Cannot get subroddits by a moderator', 'params' => $get, 'exception' => $ex]
            );
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="name", in="query", required=true, type="string")
     * @SWG\Response(response=200, description="Subroddit returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="Subroddit not found")
     * @SWG\Tag(name="Subroddit")
     * @Route("/api/subroddits/name", name="get_subroddit_by_name", methods={"GET"})
     */
    public function getSubrodditByName(Request $request, SubrodditRepository $subrodditRepository): Response
    {
        $get = $this->getGetParams($request);

        try {
            $subroddit = $subrodditRepository->findOneBy(['name' => $get['name']]);
            $serializedEntity = $this->serializer->serialize(
                $subroddit,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentSubrodditException $ex) {
            $this->logError(['message' => 'Cannot get subroddit by name', 'params' => $get, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot get subroddit by name', 'params' => $get, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="subrodditId", in="path", required=true, type="integer")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Subroddit\UpdateInput::class)
     * )
     * @SWG\Response(response=200, description="Subroddit updated")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="Subroddit not found")
     * @SWG\Tag(name="Subroddit")
     * @Route(
     *     "/api/subroddits/update/{subrodditId}",
     *     name="update_subroddit_by_id",
     *     requirements={"subrodditId": "\d+"},
     *     methods={"PUT"}
     * )
     */
    public function updateSubrodditById(Request $request, SubrodditService $subrodditService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $post['id'] = $request->get('subrodditId');
            $subroddit = $subrodditService->update($post);
            $serializedEntity = $this->serializer->serialize(
                $subroddit,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Subroddit::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentSubrodditException $ex) {
            $this->logError(['message' => 'Cannot update subroddit', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot update subroddit', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }
}
