<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\NonExistentUserException;
use App\Repository\UserRepository;
use App\Service\UserService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class UserController extends AbstractController
{
    /**
     * @SWG\Parameter(name="email", in="query", required=true, type="string")
     * @SWG\Response(response=200, description="User returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="User not found")
     * @SWG\Tag(name="User")
     * @Route("/api/users/email", name="get_user_by_email", methods={"GET"})
     */
    public function getUserByEmail(Request $request, UserRepository $userRepository): Response
    {
        $get = $this->getGetParams($request);

        try {
            $user = $userRepository->findOneBy(['email' => $get['email']]);
            $serializedEntity = $this->serializer->serialize(
                $user,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => User::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentUserException $ex) {
            $this->logError(['message' => 'Cannot get user by email', 'params' => $get, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot get user by email', 'params' => $get, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="userId", in="path", required=true, type="integer")
     * @SWG\Response(response=200, description="User returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="User not found")
     * @SWG\Tag(name="User")
     * @Route("/api/users/id/{userId}", name="get_user_by_id", methods={"GET"})
     */
    public function getUserById(Request $request, UserRepository $userRepository): Response
    {
        $id = $request->get('userId');

        try {
            $user = $userRepository->findOneBy(['id' => $id]);
            $serializedEntity = $this->serializer->serialize(
                $user,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => User::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentUserException $ex) {
            $this->logError(['message' => 'Cannot get user by id', 'params' => $id, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot get user by id', 'params' => $id, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\User\RegisterInput::class)
     * )
     * @SWG\Response(response=201, description="User registered")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="User")
     * @Route("/api/users/register", name="user_register", methods={"POST"})
     */
    public function register(Request $request, UserService $userService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $userService->create($post);

            $response = new Response(json_encode(['message' => 'User successfully registered']), Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot register user', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="userId", in="path", required=true, type="integer")
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\User\UpdateInput::class)
     * )
     * @SWG\Response(response=200, description="User updated")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="User not found")
     * @SWG\Tag(name="User")
     * @Route("/api/users/update/{userId}", name="update_user_by_id", requirements={"userId": "\d+"}, methods={"PUT"})
     */
    public function updateUserById(Request $request, UserService $userService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $post['id'] = $request->get('userId');
            $user = $userService->update($post);
            $serializedEntity = $this->serializer->serialize(
                $user,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => User::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentUserException $ex) {
            $this->logError(['message' => 'Cannot update user by id', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot update user by id', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="username", in="query", required=true, type="string")
     * @SWG\Response(response=200, description="User returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Response(response=404, description="User not found")
     * @SWG\Tag(name="User")
     * @Route("/api/users/username", name="get_user_by_username", methods={"GET"})
     */
    public function getUserByUsername(Request $request, UserRepository $userRepository): Response
    {
        $get = $this->getGetParams($request);

        try {
            $user = $userRepository->findOneBy(['username' => $get['username']]);
            $serializedEntity = $this->serializer->serialize(
                $user,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => User::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntity]), Response::HTTP_OK);
        } catch (NonExistentUserException $ex) {
            $this->logError(['message' => 'Cannot get user by username', 'params' => $get, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => $ex->getMessage()]), Response::HTTP_NOT_FOUND);
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot get user by username', 'params' => $get, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }
}
