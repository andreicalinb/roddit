<?php

namespace App\Controller;

use App\Entity\Subscription;
use App\Repository\SubscriptionRepository;
use App\Service\SubscriptionService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class SubscriptionController extends AbstractController
{
    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Subscription\FlairInput::class)
     * )
     * @SWG\Response(response=200, description="Flair updated")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Subscription")
     * @Route("/api/subscriptions/flair", name="update_flair_by_subscription", methods={"POST"})
     */
    public function updateFlairBySubscription(Request $request, SubscriptionService $subscriptionService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $subscriptionService->updateFlair($post);

            $response = new Response(json_encode(['message' => 'Flair successfully updated']), Response::HTTP_OK);
        } catch (\Exception $ex) {
            $this->logError(
                ['message' => 'Cannot update the flair of a subscription', 'params' => $post, 'exception' => $ex]
            );
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Subscription\SubscribeInput::class)
     * )
     * @SWG\Response(response=200, description="User subscribed")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Subscription")
     * @Route("/api/subscriptions/subscribe", name="subscribe_user_to_subroddit", methods={"POST"})
     */
    public function subscribe(Request $request, SubscriptionService $subscriptionService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $subscriptionService->subscribe($post);

            $response = new Response(
                json_encode(['message' => 'User successfully subscribed to subroddit']),
                Response::HTTP_OK
            );
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot subscribe user', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="object",
     *     @Model(type=\App\DTO\Subscription\UnsubscribeInput::class)
     * )
     * @SWG\Response(response=200, description="User unsubscribed")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Subscription")
     * @Route("/api/subscriptions/unsubscribe", name="unsubscribe_user_from_subroddit", methods={"POST"})
     */
    public function unsubscribe(Request $request, SubscriptionService $subscriptionService): Response
    {
        $post = $this->getPostParams($request);

        try {
            $subscriptionService->unsubscribe($post);

            $response = new Response(
                json_encode(['message' => 'User successfully unsubscribed from subroddit']),
                Response::HTTP_OK
            );
        } catch (\Exception $ex) {
            $this->logError(['message' => 'Cannot unsubscribe user', 'params' => $post, 'exception' => $ex]);
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * @SWG\Parameter(name="userId", in="path", required=true, type="integer")
     * @SWG\Parameter(name="page", in="query", default="1", required=true, type="integer")
     * @SWG\Parameter(name="itemsPerPage", in="query", default="8", required=true, type="integer")
     * @SWG\Response(response=200, description="Subscriptions returned")
     * @SWG\Response(response=400, description="Bad request")
     * @SWG\Tag(name="Subscription")
     * @Route(
     *     "/api/subscriptions/user/{userId}",
     *     name="get_subscriptions_by_user",
     *     requirements={"userId": "\d+"},
     *     methods={"GET"}
     * )
     */
    public function getSubscriptionsByUser(Request $request, SubscriptionRepository $subscriptionRepository): Response
    {
        $get = $this->getGetParams($request);
        $params = $this->getRouteParams($request);

        try {
            $subscriptions = $subscriptionRepository->findBy(
                ['user' => $params['userId']],
                [],
                $get['itemsPerPage'],
                ($get['page'] - 1) * $get['itemsPerPage']
            );
            $serializedEntities = $this->serializer->serialize(
                $subscriptions,
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => Subscription::GROUPS_GET]
            );

            $response = new Response(json_encode(['message' => $serializedEntities]), Response::HTTP_OK);
        } catch (\Exception $ex) {
            $this->logError(
                ['message' => 'Cannot get subscriptions by an user', 'params' => $params, 'exception' => $ex]
            );
            $response = new Response(json_encode(['message' => self::MESSAGE_BAD_REQUEST]), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }
}
