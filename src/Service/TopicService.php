<?php

namespace App\Service;

use App\Entity\Topic;
use App\Exception\InvalidEntityException;
use App\Exception\NonExistentSubrodditException;
use App\Exception\NonExistentUserException;
use App\Repository\SubrodditRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TopicService extends AbstractService
{
    private SubrodditRepository $subrodditRepository;

    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $entityManager, SubrodditRepository $subrodditRepository, UserRepository $userRepository, ValidatorInterface $validator)
    {
        parent::__construct($entityManager, $validator);
        $this->subrodditRepository = $subrodditRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidEntityException | NonExistentSubrodditException | NonExistentUserException
     */
    public function create(array $data): Topic
    {
        $subroddit = $this->subrodditRepository->findOneBy(['id' => $data['subrodditId']]);
        $user = $this->userRepository->findOneBy(['id' => $data['userId']]);

        $topic = new Topic($user, $subroddit);
        $topic->setTitle($data['title']);
        $topic->setDescription($data['description'] ?? '');
        $topic->setCreated(new \DateTime());

        $validationErrors = $this->validateEntity($topic);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($topic);

        return $topic;
    }
}
