<?php

namespace App\Service;

use App\Entity\AbstractEntity;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AbstractService
{
    protected ObjectManager $entityManager;

    protected ValidatorInterface $validator;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    protected function validateEntity(AbstractEntity $entity): array
    {
        $violations = $this->validator->validate($entity);

        $validationErrors = [];
        foreach ($violations as $violation) {
            /** @var ConstraintViolation $violation */
            $validationErrors[] = $violation->getMessage();
        }

        return $validationErrors;
    }

    protected function saveEntity(AbstractEntity $entity): void
    {
        //Entities that are not new should not be persisted
        if (!$this->entityManager->contains($entity)) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    protected function deleteEntity(AbstractEntity $entity): void
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}
