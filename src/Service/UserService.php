<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\InvalidEntityException;
use App\Exception\NonExistentUserException;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService extends AbstractService
{
    private UserPasswordEncoderInterface $encoder;

    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder, UserRepository $userRepository, ValidatorInterface $validator)
    {
        parent::__construct($entityManager, $validator);
        $this->encoder = $encoder;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidEntityException
     */
    public function create(array $data): User
    {
        $now = new \DateTime();
        $user = new User();
        $user->setEmail($data['email']);
        $user->setUsername($data['username']);
        $user->setPassword($this->encoder->encodePassword($user, $data['password']));
        $user->setIsActive(true);
        $user->setCreated($now);
        $user->setModified($now);

        $validationErrors = $this->validateEntity($user);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($user);

        return $user;
    }

    /**
     * @throws InvalidEntityException | NonExistentUserException
     */
    public function update(array $data): User
    {
        $user = $this->userRepository->findOneBy(['id' => $data['id']]);

        $password = isset($data['password']) ?
            $this->encoder->encodePassword($user, $data['password']) : $user->getPassword();
        $user->setPassword($password);
        $user->setEmail($data['email'] ?? $user->getEmail());

        $validationErrors = $this->validateEntity($user);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($user);

        return $user;
    }
}
