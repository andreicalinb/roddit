<?php

namespace App\Service;

use App\Entity\Subscription;
use App\Exception\InvalidEntityException;
use App\Exception\NonExistentSubrodditException;
use App\Exception\NonExistentSubscriptionException;
use App\Exception\NonExistentUserException;
use App\Repository\SubrodditRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SubscriptionService extends AbstractService
{
    private SubscriptionRepository $subscriptionRepository;

    private SubrodditRepository $subrodditRepository;

    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $entityManager, SubscriptionRepository $subscriptionRepository, SubrodditRepository $subrodditRepository, UserRepository $userRepository, ValidatorInterface $validator)
    {
        parent::__construct($entityManager, $validator);
        $this->subscriptionRepository = $subscriptionRepository;
        $this->subrodditRepository = $subrodditRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidEntityException
     * @throws NonExistentSubrodditException
     * @throws NonExistentUserException
     */
    public function subscribe(array $data): Subscription
    {
        $user = $this->userRepository->findOneBy(['id' => $data['userId']]);
        $subroddit = $this->subrodditRepository->findOneBy(['id' => $data['subrodditId']]);
        $subscription = new Subscription($user, $subroddit);

        $validationErrors = $this->validateEntity($subscription);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($subscription);

        return $subscription;
    }

    /**
     * @throws NonExistentSubscriptionException
     */
    public function unsubscribe(array $data): void
    {
        $subscription = $this->subscriptionRepository->findOneBy(
            ['subrodditId' => $data['subrodditId'], 'userId' => $data['userId']]
        );

        $this->deleteEntity($subscription);
    }

    /**
     * @throws NonExistentSubscriptionException
     */
    public function updateFlair(array $data): Subscription
    {
        $subscription = $this->subscriptionRepository->findOneBy(
            ['subrodditId' => $data['subrodditId'], 'userId' => $data['userId']]
        );

        $subscription->setFlair($data['flair']);

        $this->saveEntity($subscription);

        return $subscription;
    }
}
