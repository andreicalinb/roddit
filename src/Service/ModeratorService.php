<?php

namespace App\Service;

use App\Entity\Moderator;
use App\Exception\InvalidEntityException;
use App\Exception\NonExistentModeratorException;
use App\Exception\NonExistentSubrodditException;
use App\Exception\NonExistentUserException;
use App\Repository\ModeratorRepository;
use App\Repository\SubrodditRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ModeratorService extends AbstractService
{
    private ModeratorRepository $moderatorRepository;

    private SubrodditRepository $subrodditRepository;

    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $entityManager, ModeratorRepository $moderatorRepository, SubrodditRepository $subrodditRepository, UserRepository $userRepository, ValidatorInterface $validator)
    {
        parent::__construct($entityManager, $validator);
        $this->moderatorRepository = $moderatorRepository;
        $this->subrodditRepository = $subrodditRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidEntityException | NonExistentSubrodditException | NonExistentUserException
     */
    public function register(array $data): Moderator
    {
        $subroddit = $this->subrodditRepository->findOneBy(['id' => $data['subrodditId']]);
        $user = $this->userRepository->findOneBy(['id' => $data['userId']]);
        $moderator = new Moderator($user, $subroddit);

        $validationErrors = $this->validateEntity($moderator);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($moderator);

        return $moderator;
    }

    /**
     * @throws NonExistentModeratorException
     */
    public function unregister(array $data): void
    {
        /** @var Moderator $moderator */
        $moderator = $this->moderatorRepository->findOneBy(
            ['subrodditId' => $data['subrodditId'], 'userId' => $data['userId']]
        );

        $this->deleteEntity($moderator);
    }
}
