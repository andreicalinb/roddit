<?php

namespace App\Service;

use App\Entity\Subroddit;
use App\Exception\InvalidEntityException;
use App\Exception\NonExistentSubrodditException;
use App\Repository\SubrodditRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SubrodditService extends AbstractService
{
    private SubrodditRepository $subrodditRepository;

    public function __construct(EntityManagerInterface $entityManager, SubrodditRepository $subrodditRepository, ValidatorInterface $validator)
    {
        parent::__construct($entityManager, $validator);
        $this->subrodditRepository = $subrodditRepository;
    }

    /**
     * @throws InvalidEntityException
     */
    public function create(array $data): Subroddit
    {
        $subroddit = new Subroddit();
        $subroddit->setName($data['name'])
            ->setShortDesc($data['shortDesc']);

        $validationErrors = $this->validateEntity($subroddit);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($subroddit);

        return $subroddit;
    }

    /**
     * @throws InvalidEntityException | NonExistentSubrodditException
     */
    public function update(array $data): Subroddit
    {
        $subroddit = $this->subrodditRepository->findOneBy(['id' => $data['id']]);

        $subroddit->setShortDesc($data['shortDesc'] ?? $subroddit->getShortDesc())
            ->setLogo($data['logo'] ?? $subroddit->getLogo());

        $validationErrors = $this->validateEntity($subroddit);
        if (!empty($validationErrors)) {
            throw new InvalidEntityException(implode(',', $validationErrors));
        }

        $this->saveEntity($subroddit);

        return $subroddit;
    }
}
