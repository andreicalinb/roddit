<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionRepository")
 * @ORM\Table(name="subscriptions")
 *
 * @UniqueEntity(fields={"subroddit", "user"}, message="User is already subscribed to the subroddit")
 */
class Subscription extends AbstractEntity
{
    const GROUPS_GET = 'get';
    const GROUPS_SUBSCRIBE = 'subscribe';
    const GROUPS_UNSUBSCRIBE = 'unsubscribe';
    const GROUPS_UPDATE_FLAIR = 'update_flair';

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="subscriptions", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     *
     * @Groups({"get", "subscribe", "unsubscribe", "update_flair"})
     */
    private User $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Subroddit", inversedBy="subscriptions", fetch="EAGER")
     * @ORM\JoinColumn(name="subroddit_id", nullable=false, fieldName="subroddit")
     *
     * @Groups({"get", "subscribe", "unsubscribe", "update_flair"})
     */
    private Subroddit $subroddit;

    /**
     * @ORM\Column(name="since", type="datetime")
     *
     * @Groups({"get"})
     */
    private DateTimeInterface $since;

    /**
     * @Assert\Length(max="30", maxMessage="Subscription flair is too long")
     *
     * @ORM\Column(name="flair", type="string", length=30)
     *
     * @Groups({"get", "update_flair"})
     */
    private string $flair;

    public function __construct(User $user, Subroddit $subroddit)
    {
        $this->flair = '';
        $this->since = new \DateTime();
        $this->subroddit = $subroddit;
        $this->user = $user;
    }

    public function getSince(): DateTimeInterface
    {
        return $this->since;
    }

    public function setSince(DateTimeInterface $since): void
    {
        $this->since = $since;
    }

    public function getFlair(): string
    {
        return $this->flair;
    }

    public function setFlair(string $flair): void
    {
        $this->flair = $flair;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getSubroddit(): Subroddit
    {
        return $this->subroddit;
    }

    public function setSubroddit(Subroddit $subroddit): void
    {
        $this->subroddit = $subroddit;
    }
}
