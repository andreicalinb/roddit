<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TopicRepository")
 * @ORM\Table(name="topics")
 */
class Topic extends AbstractEntity
{
    const GROUPS_CREATE = 'create';
    const GROUPS_GET = 'get';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="topics")
     * @ORM\JoinColumn(name="op_id", nullable=false)
     *
     * @Groups({"create"})
     */
    private User $op;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subroddit", inversedBy="topics")
     * @ORM\JoinColumn(name="subroddit_id", nullable=false)
     *
     * @Groups({"create"})
     */
    private Subroddit $subroddit;

    /**
     * @Assert\Length(max="100", maxMessage="Topic title is too long")
     *
     * @ORM\Column(name="title", type="string", length=100)
     *
     * @Groups({"create", "get"})
     */
    private string $title;

    /**
     * @Assert\Length(max="255", maxMessage="Topic description is too long")
     *
     * @ORM\Column(name="description", type="string", length=255)
     *
     * @Groups({"create", "get"})
     */
    private string $description;

    /**
     * @ORM\Column(name="created", type="datetime")
     *
     * @Groups({"get"})
     */
    private DateTimeInterface $created;

    public function __construct(User $user, Subroddit $subroddit)
    {
        $this->created = new \DateTime();
        $this->description = '';
        $this->id = 0;
        $this->op = $user;
        $this->subroddit = $subroddit;
        $this->title = '';
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getOp(): User
    {
        return $this->op;
    }

    public function setOp(User $op): void
    {
        $this->op = $op;
    }

    public function getSubroddit(): Subroddit
    {
        return $this->subroddit;
    }

    public function setSubroddit(Subroddit $subroddit): void
    {
        $this->subroddit = $subroddit;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): void
    {
        $this->created = $created;
    }
}
