<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubrodditRepository")
 * @ORM\Table(name="subroddits")
 *
 * @UniqueEntity("name", message="Name is already taken")
 */
class Subroddit extends AbstractEntity
{
    const GROUPS_CREATE = 'create';
    const GROUPS_GET = 'get';
    const GROUPS_UPDATE = 'update';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     *
     * @Groups({"get"})
     */
    private int $id;

    /**
     * @Assert\Length(max="30", maxMessage="Subroddit name is too long")
     *
     * @ORM\Column(name="name", type="string", length=30, options={"fixed": true})
     *
     * @Groups({"create", "get"})
     */
    private string $name;

    /**
     * @Assert\Length(max="255", maxMessage="Subroddit short description is too long")
     *
     * @ORM\Column(name="short_desc", type="string", length=255)
     *
     * @Groups({"create", "get", "update"})
     */
    private string $shortDesc;

    /**
     * @Assert\Length(max="255", maxMessage="Subroddit logo is too long")
     *
     * @ORM\Column(name="logo", type="string", length=255)
     *
     * @Groups({"get", "update"})
     */
    private string $logo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Moderator", mappedBy="subroddit_id", orphanRemoval=true)
     */
    private Collection $moderators;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription", mappedBy="subroddit_id", orphanRemoval=true)
     */
    private Collection $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Topic", mappedBy="subroddit_id")
     */
    private Collection $topics;

    public function __construct()
    {
        $this->id = 0;
        $this->logo = '';
        $this->moderators = new ArrayCollection();
        $this->name = '';
        $this->shortDesc = '';
        $this->subscriptions = new ArrayCollection();
        $this->topics = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortDesc(): string
    {
        return $this->shortDesc;
    }

    /**
     * @return $this
     */
    public function setShortDesc(string $shortDesc): self
    {
        $this->shortDesc = $shortDesc;

        return $this;
    }

    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @return $this
     */
    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @psalm-suppress MismatchingDocblockReturnType
     *
     * @return Collection|Moderator[]
     */
    public function getModerators(): Collection
    {
        return $this->moderators;
    }

    /**
     * @return $this
     */
    public function addModerator(Moderator $moderator): self
    {
        if (!$this->moderators->contains($moderator)) {
            $this->moderators[] = $moderator;
            $moderator->setSubroddit($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeModerator(Moderator $moderator): self
    {
        if ($this->moderators->contains($moderator)) {
            $this->moderators->removeElement($moderator);
        }

        return $this;
    }

    /**
     * @psalm-suppress MismatchingDocblockReturnType
     *
     * @return Collection|Subscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    /**
     * @return $this
     */
    public function addSubscription(Subscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setSubroddit($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSubscription(Subscription $subscription): self
    {
        if ($this->subscriptions->contains($subscription)) {
            $this->subscriptions->removeElement($subscription);
        }

        return $this;
    }

    /**
     * @psalm-suppress MismatchingDocblockReturnType
     *
     * @return Collection|Topic[]
     */
    public function getTopics(): Collection
    {
        return $this->topics;
    }

    public function addTopic(Topic $topic): self
    {
        if (!$this->topics->contains($topic)) {
            $this->topics[] = $topic;
            $topic->setSubroddit($this);
        }

        return $this;
    }

    public function removeTopic(Topic $topic): self
    {
        if ($this->topics->contains($topic)) {
            $this->topics->removeElement($topic);
        }

        return $this;
    }
}
