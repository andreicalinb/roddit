<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(
 *     name="users",
 *     indexes={
 *         @ORM\Index(name="IDX_USERNAME", columns={"username"}),
 *         @ORM\Index(name="IDX_EMAIL", columns={"email"}
 *         )
 *     })
 *
 *     @UniqueEntity("username", message="Username is already taken")
 *     @UniqueEntity("email", message="An account with this email already exists")
 */
class User extends AbstractEntity implements UserInterface
{
    const GROUPS_GET = 'get';
    const GROUPS_REGISTER = 'register';
    const GROUPS_UPDATE = 'update';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     *
     * @Groups({"get"})
     */
    private int $id;

    /**
     * @Assert\Length(max="30", maxMessage="User username is too long")
     *
     * @ORM\Column(name="username", type="string", length=30, options={"fixed": true})
     *
     * @Groups({"get", "register"})
     */
    private string $username;

    /**
     * @Assert\Length(max="255", maxMessage="User password is too long")
     *
     * @ORM\Column(name="password", type="string", length=255)
     *
     * @Groups({"register", "update"})
     */
    private string $password;

    /**
     * @Assert\Length(max="30", maxMessage="User email is too long")
     * @Assert\Email(message="Invalid user email format")
     *
     * @ORM\Column(name="email", type="string", length=30, options={"fixed": true})
     *
     * @Groups({"get", "register", "update"})
     */
    private string $email;

    /**
     * @ORM\Column(name="created", type="datetime")
     *
     * @Groups({"get"})
     */
    private DateTimeInterface $created;

    /**
     * @ORM\Column(name="modified", type="datetime")
     *
     * @Groups({"get"})
     */
    private DateTimeInterface $modified;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     *
     * @Groups({"get"})
     */
    private bool $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Moderator", mappedBy="user_id", orphanRemoval=true)
     */
    private Collection $moderators;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription", mappedBy="user_id", orphanRemoval=true)
     */
    private Collection $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Topic", mappedBy="op_id")
     */
    private Collection $topics;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->email = '';
        $this->id = 0;
        $this->isActive = true;
        $this->modified = new \DateTime();
        $this->password = '';
        $this->username = '';
        $this->moderators = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->topics = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): void
    {
        $this->created = $created;
    }

    public function getModified(): ?DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(DateTimeInterface $modified): void
    {
        $this->modified = $modified;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        // Not needed for the moment
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt(): string
    {
        // Not needed since modern encoders store the sale value as part of the encoded password string
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
        // Not needed for the moment
    }

    /**
     * @psalm-suppress MismatchingDocblockReturnType
     *
     * @return Collection|Moderator[]
     */
    public function getModerators(): Collection
    {
        return $this->moderators;
    }

    public function addModerator(Moderator $moderator): void
    {
        if (!$this->moderators->contains($moderator)) {
            $this->moderators[] = $moderator;
            $moderator->setUser($this);
        }
    }

    public function removeModerator(Moderator $moderator): void
    {
        if ($this->moderators->contains($moderator)) {
            $this->moderators->removeElement($moderator);
        }
    }

    /**
     * @psalm-suppress MismatchingDocblockReturnType
     *
     * @return Collection|Subscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): void
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setUser($this);
        }
    }

    public function removeSubscription(Subscription $subscription): void
    {
        if ($this->subscriptions->contains($subscription)) {
            $this->subscriptions->removeElement($subscription);
        }
    }

    /**
     * @psalm-suppress MismatchingDocblockReturnType
     *
     * @return Collection|Topic[]
     */
    public function getTopics(): Collection
    {
        return $this->topics;
    }

    public function addTopic(Topic $topic): void
    {
        if (!$this->topics->contains($topic)) {
            $this->topics[] = $topic;
            $topic->setOp($this);
        }
    }

    public function removeTopic(Topic $topic): void
    {
        if ($this->topics->contains($topic)) {
            $this->topics->removeElement($topic);
        }
    }
}
