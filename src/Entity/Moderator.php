<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModeratorRepository")
 * @ORM\Table(name="moderators")
 *
 * @UniqueEntity(fields={"subrodditId", "userId"}, message="User is already a moderator")
 */
class Moderator extends AbstractEntity
{
    const GROUPS_GET = 'get';
    const GROUPS_REGISTER = 'register';
    const GROUPS_UNREGISTER = 'unregister';

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="moderators", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     *
     * @Groups({"get", "register", "unregister"})
     */
    private User $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Subroddit", inversedBy="moderators", fetch="EAGER")
     * @ORM\JoinColumn(name="subroddit_id", nullable=false)
     *
     * @Groups({"get", "register", "unregister"})
     */
    private Subroddit $subroddit;

    /**
     * @ORM\Column(name="since", type="datetime")
     *
     * @Groups({"get"})
     */
    private DateTimeInterface $since;

    public function __construct(User $user, Subroddit $subroddit)
    {
        $this->since = new \DateTime();
        $this->subroddit = $subroddit;
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSubroddit(): Subroddit
    {
        return $this->subroddit;
    }

    /**
     * @return $this
     */
    public function setSubroddit(Subroddit $subroddit): self
    {
        $this->subroddit = $subroddit;

        return $this;
    }

    public function getSince(): DateTimeInterface
    {
        return $this->since;
    }

    /**
     * @return $this
     */
    public function setSince(DateTimeInterface $since): self
    {
        $this->since = $since;

        return $this;
    }
}
