<?php

namespace App\DTO\Subroddit;

/**
 * @psalm-suppress MissingConstructor
 */
final class UpdateInput
{
    /**
     * @var string
     */
    public $shortDesc;

    /**
     * @var string
     */
    public $logo;
}
