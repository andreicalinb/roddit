<?php

namespace App\DTO\Subroddit;

/**
 * @psalm-suppress MissingConstructor
 */
final class CreateInput
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $shortDesc;
}
