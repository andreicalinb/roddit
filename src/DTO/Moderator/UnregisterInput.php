<?php

namespace App\DTO\Moderator;

/**
 * @psalm-suppress MissingConstructor
 */
final class UnregisterInput
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $subrodditId;
}
