<?php

namespace App\DTO\Moderator;

/**
 * @psalm-suppress MissingConstructor
 */
final class RegisterInput
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $subrodditId;
}
