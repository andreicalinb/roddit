<?php

namespace App\DTO\User;

/**
 * @psalm-suppress MissingConstructor
 */
final class RegisterInput
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $subrodditId;
}
