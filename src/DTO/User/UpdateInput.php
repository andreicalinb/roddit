<?php

namespace App\DTO\User;

/**
 * @psalm-suppress MissingConstructor
 */
final class UpdateInput
{
    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $email;
}
