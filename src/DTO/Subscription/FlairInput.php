<?php

namespace App\DTO\Subscription;

/**
 * @psalm-suppress MissingConstructor
 */
final class FlairInput
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $subrodditId;

    /**
     * @var string
     */
    public $flair;
}
