<?php

namespace App\DTO\Subscription;

/**
 * @psalm-suppress MissingConstructor
 */
final class UnsubscribeInput
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $subrodditId;
}
