<?php

namespace App\DTO\Topic;

/**
 * @psalm-suppress MissingConstructor
 */
final class CreateInput
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $subrodditId;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;
}
