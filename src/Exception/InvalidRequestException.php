<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class InvalidRequestException extends \Exception
{
    public function __construct(string $message, int $code = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct($message, $code);
    }
}
