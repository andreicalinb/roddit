<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class NonExistentSubrodditException extends \Exception
{
    public function __construct(string $message = '', int $code = Response::HTTP_NOT_FOUND)
    {
        $message = trim('Subroddit does not exist. '.$message);
        parent::__construct($message, $code);
    }
}
