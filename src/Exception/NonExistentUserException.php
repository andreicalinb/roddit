<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class NonExistentUserException extends \Exception
{
    public function __construct(string $message = '', int $code = Response::HTTP_NOT_FOUND)
    {
        $message = trim('User does not exist. '.$message);
        parent::__construct($message, $code);
    }
}
