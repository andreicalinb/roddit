const baseState = { categories: [] };

export default (state = baseState, action) => {
  switch (action.type) {
    case 'FETCH_CATEGORIES':
      return { ...state, categories: action.payload }
    default:
      return state;
  }
}
