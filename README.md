# What

This is the ROmanian rebranding copy of Reddit.

# Why
The scope was to improve my Symfony skill and use various libraries.

# How to install and configure
0. Make sure you have installed Docker and docker-compose (I have `Docker version 17.05.0-ce` and `docker-compose version 1.22.0`)
1. Close the repository in `/var/www/html/ROddit` (yes, with capital `RO`)
2. Create the containers `docker-compose up -d`
3. Install BE dependencies `docker exec -it docker-symfony4 composer install`
4. Install FE dependencies `docker exec -it docker-symfony4 yarn install`
5. Front page should work `http://localhost:8000/`
6. API documentation should work `http://localhost:8000/api/doc`

# BE cheatsheet
  * Install BE dependencies changes: `docker exec -it docker-symfony4 composer install`
  * Run symfony console: `docker exec -it docker-symfony4 php bin/console`
  * Generate migrations: `docker exec -it docker-symfony4 php bin/console make:migration`
  * Apply migrations: `docker exec -it docker-symfony4 php bin/console doctrine:migrations:migrate`
  * Clear cache: `docker exec -it docker-symfony4 php bin/console cache:clear`

# FE cheatsheet
  * Install BE dependencies changes: `docker exec -it docker-symfony4 yarn install`
  * Compile frontend: `docker exec -it docker-symfony4 yarn encore dev`

# Docker compose cheatsheet
  * Start containers in the background: `docker-compose up -d`
  * Start containers on the foreground: `docker-compose up`
  * Stop containers: `docker-compose stop`
  * Kill containers: `docker-compose kill`
  * View container logs: `docker-compose logs`
  * Open a Mysql shell: `docker-compose exec mysql mysql -uroot -p`
  * Enter Symfony/React container: `docker exec -it docker-symfony4 bash`
  * Enter Mysql container: `docker exec -it docker-mysql bash`
  * Enter Nginx container: `docker exec -it docker-nginx bash`

# Code quality
  * PHP cs fixer - check for errors: `docker exec -it docker-symfony4 php vendor/bin/php-cs-fixer fix -v --dry-run`
  * PHP cs fixer - automatically fix errors: `docker exec -it docker-symfony4 php vendor/bin/php-cs-fixer fix`
  * PHPStan - check for bugs: `docker exec -it docker-symfony4 php vendor/bin/phpstan analyse --no-progress --no-interaction`
  * Psalm - check for bugs: `docker exec -it docker-symfony4 php vendor/bin/psalm --no-progress --show-info=false src/`
  * All: `docker exec -it docker-symfony4 php vendor/bin/php-cs-fixer fix && docker exec -it docker-symfony4 php vendor/bin/phpstan analyse --no-progress --no-interaction && docker exec -it docker-symfony4 php vendor/bin/psalm --no-progress --show-info=false src/`
